//---------------------CONSTANTS--------------------
const NODE_WIDTH = 150;
const NODE_HEIGHT = 100;
const LABEL_WIDTH = 90;
const LABEL_HEIGHT = 20;


//---------------------CLASS--------------------
export default class SimpleLayouter {


    //---------------------ATTRIBUTES---------------------
    public currentLayoutedNodes: Array<any>;
    public currentLayoutedEdges: Array<any>;

    
    //---------------------CONSTRUCTOR---------------------
    constructor() {
        
        this.currentLayoutedNodes = [];
        this.currentLayoutedEdges = [];
        
    }
    
    
    //---------------------METHODS---------------------
    /**
     * Layouts a given node in a side by side layout. Returns a di xml tag with the bounds of an node.
     * 
     * @param node The node to layout
     * @returns {string} The di string
     */
    layoutNodeSideBySide(node: any) {

        this.currentLayoutedNodes.push(
            { 
                node: node, 
                bounds: { 
                    x: (this.currentLayoutedNodes.length * NODE_WIDTH) + (this.currentLayoutedNodes.length * NODE_WIDTH) + NODE_WIDTH,
                    y: (NODE_HEIGHT) 
                }
            }
        );
        
        return `<dc:Bounds x="${this.getNode(node).x}" y="${this.getNode(node).y}" width="${NODE_WIDTH}" height="${NODE_HEIGHT}"/>`;
        
    }

    /**
     * Layouts a given edge in a side by side layout. Returns a di xml tag with the waypoints of the edge.
     * 
     * @param edge The edge to layout
     * @returns {string} The di string
     */
    layoutEdgeSideBySide(edge: any) {
        
        if(edge.type === 'GENERALISATION') {

            this.currentLayoutedEdges.push(
                {
                    edge: edge,
                    waypoints: {
                        source: { x: this.getNode(edge.source).x, y: this.getNode(edge.source).y + (NODE_HEIGHT / 2) },
                        target: { x: this.getNode(edge.target).x + NODE_WIDTH, y: this.getNode(edge.target).y + (NODE_HEIGHT / 2)}
                    }
                }
            );

        } else {

            this.currentLayoutedEdges.push(
                {
                    edge: edge,
                    waypoints: {
                        source: { x: this.getNode(edge.source).x + NODE_WIDTH, y: this.getNode(edge.source).y + (NODE_HEIGHT / 2) },
                        target: { x: this.getNode(edge.target).x, y: this.getNode(edge.target).y + (NODE_HEIGHT / 2)}
                    }
                }
            );
        }
        
        return `<di:waypoint xsi:type="dc:Point" x="${this.getEdge(edge).source.x}" y="${this.getEdge(edge).source.y}"/>
                <di:waypoint xsi:type="dc:Point" x="${this.getEdge(edge).target.x}" y="${this.getEdge(edge).target.y}"/>`;
        
    }

    /**
     * Layouts the labels of a given edge. Returns a di xml tag with the label there bounces.
     * 
     * @param edge The edge with the labels to layout
     * @param edgeIndex The index of this edge, is used for the label id
     * @returns {string} The di string
     */
    layoutLabelSideBySide(edge: any, edgeIndex: number) {
        
        return `<umldi:UMLLabel id="UMLLabel_${edgeIndex}_0">
                    <dc:Bounds x="${this.getEdge(edge).source.x + (LABEL_HEIGHT / 2)}" y="${this.getEdge(edge).source.y - LABEL_HEIGHT}" width="${LABEL_WIDTH}" height="${LABEL_HEIGHT}"/>
                </umldi:UMLLabel>
                <umldi:UMLLabel id="UMLLabel_${edgeIndex}_1">
                    <dc:Bounds x="${this.getEdge(edge).source.x + (LABEL_HEIGHT / 2)}" y="${this.getEdge(edge).source.y}" width="${LABEL_WIDTH}" height="${LABEL_HEIGHT}"/>
                </umldi:UMLLabel>
                <umldi:UMLLabel id="UMLLabel_${edgeIndex}_2">
                    <dc:Bounds x="${this.getEdge(edge).target.x - 100}" y="${this.getEdge(edge).target.y - LABEL_HEIGHT}" width="${LABEL_WIDTH}" height="${LABEL_HEIGHT}"/>
                </umldi:UMLLabel>
                <umldi:UMLLabel id="UMLLabel_${edgeIndex}_3">
                    <dc:Bounds x="${this.getEdge(edge).target.x - 100}" y="${this.getEdge(edge).target.y}" width="${LABEL_WIDTH}" height="${LABEL_HEIGHT}"/>
                </umldi:UMLLabel>`;
        
    }
    
    /**
     * Clears the layouter for a new diagram
     */
    clearLayouter() {
        
        this.currentLayoutedNodes = [];
        
    }

    /**
     * Returns the position from the layout nodes array
     * 
     * @param node The node to looking for
     * @returns {any} The bounds
     */
    private getNode(node: any) {
        
        for(let e of this.currentLayoutedNodes) {
            
            if(e.node.id === node.id) {
         
                return e.bounds;
                
            }
        }
        
        return null;
        
    }

    /**
     * Returns the waypoints from the layout edges array
     * 
     * @param edge The edge to looking for
     * @returns {any} The waypoints
     */
    private getEdge(edge: any) {
        
        for(let e of this.currentLayoutedEdges) {
            
            if(e.edge.source.id === edge.source.id && e.edge.target.id === edge.target.id) {
                
                return e.waypoints;
                
            }
        }
        
        return null;
        
    }
}
