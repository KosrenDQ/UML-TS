//---------------------IMPORTS---------------------
import SimpleLayout from './SimpleLayouter';

//---------------------EXPORTS---------------------
module.exports = {

    simpleLayouter: [ 'type', SimpleLayout ]

};
