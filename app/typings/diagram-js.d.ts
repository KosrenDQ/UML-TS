// Type definitions for diagram-js 0.17.0
// Project: https://github.com/bpmn-io/diagram-js
// Definitions by: Jan Steinbruecker <https://github.com/3fd>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare namespace DiagramJS {

    class Diagram {
        injector: any;
        get: any;
        invoke: any;

        constructor(options, injector);
        destroy(): void;
        clear(): void;
    }

    //////////////////////////////////////////////////////

    /*************
     *  COMMAND  *
     *************/

    class CommandInterceptor {
        constructor(eventBus);
        on(events, hook, priority, handlerFn?, unwrap?, that?): void;
    }

    //////////////////////////////////////////////////////

    /**********
     *  CORE  *
     **********/

    /** The main drawing canvas. */
    class Canvas {

        /* @constructor
         * @emits Canvas#canvas.init
         * @param {Object} config
         * @param {EventBus} eventBus
         * @param {GraphicsFactory} graphicsFactory
         * @param {ElementRegistry} elementRegistry
         */
        constructor(config: any, eventBus, graphicsFactory, elementRegistry);

        protected _addElement(type: string, element: any, parent?: any, parentIndex?: number): any;

        protected _clear(): void;

        protected _destroy(emit): void;

        protected _ensureValid(type: string, element: any): void;

        protected _fitViewport(center: any): any;

        protected _init(config): void;

        protected _updateMarker(element: any, marker: string, add: boolean): void;

        protected _removeElement(element: any, type: string): any;

        protected _setParent(element: any, parent: any, parentIndex: number);

        protected _setZoom(scale: any, center: any): any;

        protected _viewboxChanged(): void;

        protected _viewboxChanging(): void;

        /**
         * Adds a connection to the canvas
         * @param {Object|djs.model.Connection} connection to add to the diagram
         * @param {djs.model.Base} [parent]
         * @param {number} [parentIndex]
         * @return {djs.model.Connection} the added connection
         */
        addConnection(connection: any, parent?: any, parentIndex?: number)

        /**
         * Adds a marker to an element (basically a css class).
         * Fires the element.marker.update event, making it possible to
         * integrate extension into the marker life-cycle, too.
         * @param {string|djs.model.Base} element
         * @param {string} marker
         */
        addMarker(element: any, marker: string): void;

        /**
         * Adds a shape to the canvas
         * @param {Object|djs.model.Shape} shape to add to the diagram
         * @param {djs.model.Base} [parent]
         * @param {number} [parentIndex]
         * @return {djs.model.Shape} the added shape
         */
        addShape(shape: any, parent?: any, parentIndex?: number): any;

        /**
         * Return the absolute bounding box for the given element
         * The absolute bounding box may be used to display overlays in the
         * callers (browser) coordinate system rather than the zoomed in/out
         * canvas coordinates.
         * @param  {ElementDescriptor} element
         * @return {Bounds} the absolute bounding box
         */
        getAbsoluteBBox(element: any): {x: number, y: number, width: number, height: number};

        /**
         * Returns the default layer on which
         * all elements are drawn.
         * @returns {Snap<SVGGroup>}
         */
        getDefaultLayer(): any;

        /**
         * Return the graphical object underlaying a certain diagram element
         * @param {string|djs.model.Base} element descriptor of the element
         * @param {boolean} [secondary=false] whether to return the secondary connected element
         * @return {SVGElement}
         */
        getGraphics(element: any, secondary?: boolean): any;

        /**
         * Returns a layer that is used to draw elements
         * or annotations on it.
         * @param {string} name
         * @returns {Snap<SVGGroup>}
         */
        getLayer(name: string): any;

        /**
         * Returns the html element that encloses the
         * drawing canvas.
         * @return {DOMNode}
         */
        getContainer(): any;

        getRootElement(): any;

        /**
         * Returns the size of the canvas
         * @return {Dimensions}
         */
        getSize(): {width: number, height: number};

        /**
         * Check the existence of a marker on element.
         * @param  {string|djs.model.Base} element
         * @param  {string} marker
         */
        hasMarker(element: any, marker: string): boolean;

        /**
         * Removes a connection from the canvas
         * @param {string|djs.model.Connection} connection or connection id to be removed
         * @return {djs.model.Connection} the removed connection
         */
        removeConnection(connection: any): any;

        /**
         * Remove a marker from an element.
         * Fires the element.marker.update event, making it possible to
         * integrate extension into the marker life-cycle, too.
         * @param  {string|djs.model.Base} element
         * @param  {string} marker
         */
        removeMarker(element: any, marker: string): void;

        /**
         * Removes a shape from the canvas
         * @param {string|djs.model.Shape} shape or shape id to be removed
         * @return {djs.model.Shape} the removed shape
         */
        removeShape(shape: any): any;

        /**
         * Gets or sets the scroll of the canvas.
         * @param {Object} [delta] the new scroll to apply.
         * @param {number} [delta.dx]
         * @param {number} [delta.dy]
         */
        scroll(delta: {dx: number, dy: number}): {x: number, y: number};

        /**
         * Sets a given element as the new root element for the canvas
         * and returns the new root element.
         * @param {Object|djs.model.Root} element
         * @param {boolean} [override] whether to override the current root element, if any
         * @return {Object|djs.model.Root} new root element
         */
        setRootElement(element: any, override: boolean): any;

        /**
         * Toggles a marker on an element.
         * Fires the element.marker.update event, making it possible to
         * integrate extension into the marker life-cycle, too.
         * @param  {string|djs.model.Base} element
         * @param  {string} marker
         */
        toggleMarker(element: any, marker: string): void;

        /**
         * Gets or sets the view box of the canvas, i.e. the
         * area that is currently displayed.
         * The getter may return a cached viewbox (if it is currently
         * changing). To force a recomputation, pass `false` as the first argument.
         * @param  {Object} [box] the new view box to set
         * @param  {number} box.x the top left X coordinate of the canvas visible in view box
         * @param  {number} box.y the top left Y coordinate of the canvas visible in view box
         * @param  {number} box.width the visible width
         * @param  {number} box.height
         * @return {Object} the current view box
         */
        viewbox(box?: {x: number, y: number, width: number, height: number}|boolean): any

        /**
         * Gets or sets the current zoom of the canvas, optionally zooming
         * to the specified position.
         * The getter may return a cached zoom level. Call it with `false` as
         * the first argument to force recomputation of the current level.
         * @param {string|number} [newScale] the new zoom level, either a number, i.e. 0.9,
         * @param {string|Point} [center] the reference point { x: .., y: ..} to zoom to, 'auto' to zoom into mid or null
         * @return {number} the current scale
         */
        zoom(newScale: string|number|boolean, center?: string|Point): number;

    }


    interface Box {

    }

    class ElementFactory {
        protected _uid;
        constructor();
        createRoot(attrs: any): Model.Base;
        createLabel(attrs: any): Model.Base;
        createShape(attrs: any): Model.Base;
        createConnection(attrs: any): Model.Base;
        create(type: string, attrs: any): Model.Base;
    }

    //////////////////////////////////////////////////////

    /**********
     *  DRAW  *
     **********/

    class BaseRenderer {
        constructor(...paras: any[]);
        canRender(element?: any): boolean;
        drawShape(visuals?: any, element?: any): any;
        drawConnection(visuals?: any, element?: any): any;
        getShapePath(): any;
        getConnectionPath(): any;
    }

    //////////////////////////////////////////////////////

    /**************************
     *  FEATURES / Connect *
     **************************/

    class Connect {

        constructor(eventBus, dragging, modeling: Modeling, rules, canvas, graphicsFactory);

        /**
         * Start connect operation.
         *
         * @param {DOMEvent} event
         * @param {djs.model.Base} source
         * @param {Point} [sourcePosition]
         * @param {Boolean} [autoActivate]
         */
        start(event: any, source: any, sourcePosition?: Point, autoActivate?: boolean): void;

    }

    /**************************
     *  FEATURES / ContextPad *
     **************************/

    class ContextPad {

        /**
         * A context pad that displays element specific, contextual actions next
         * to a diagram element.
         * @param {EventBus} eventBus
         * @param {Overlays} overlays
         */
        constructor(eventBus, overlays);

        /** Registers events needed for interaction with other components */
        _init(): void;

        _updateAndOpen(element: any): void;

        /** Close the context pad */
        close(): void;

        /**
         * Returns the context pad entries for a given element
         * @param {djs.element.Base} element
         * @return {Array<ContextPadEntryDescriptor>} list of entries
         */
        getEntries(element): any;

        getPad(element: any): any;

        /**
         * Check if pad is open. If element is given, will check
         * if pad is opened with given element.
         * @param {Element} element
         * @return {Boolean}
         */
        isOpen(element): boolean;

        /**
         * Open the context pad for the given element
         * @param {djs.model.Base} element
         * @param {Boolean} force if true, force reopening the context pad
         */
        open(element: any, force: boolean): void;

        /**
         * Register a provider with the context pad
         * @param  {ContextPadProvider} provider
         */
        registerProvider(provider): void;

        /**
         * Trigger an action available on the opened context pad
         * @param  {string} action
         * @param  {Event} event
         * @param  {boolean} [autoActivate]
         */
        trigger(action: string, event: Event, autoActivate?: boolean): any;

    }

    /**********************
     *  FEATURES / Create *
     **********************/

    class Create {
        constructor(eventBus, dragging, rules, modeling: Modeling, canvas, styles, graphicsFactory);
        start(event, shape, source): void;
    }

    /************************
     *  FEATURES / Modeling *
     ************************/

    class Modeling {
        constructor(eventBus, elementFactory, commandStack, rules?: any);
        getHandlers(): any;
        registerHandlers(commandStack): void;
        moveShape(shape, delta, newParent, newParentIndex, hints): void;
        updateAttachment(shape, newHost): void;
        moveElements(shapes, delta, target, isAttach, hints): void;
        moveConnection(connection, delta, newParent, newParentIndex, hints): void;
        layoutConnection(connection, hints): void;
        createConnection(source, target, targetIndex?, connection?, parent?, hints?): any;
        createShape(shape, position, target, targetIndex, isAttach, hints): any;
        createLabel(labelTarget, position, label, parent): any;
        appendShape(source, shape, position, parent, connection, connectionParent): any;
        removeElements(elements: any[]): void;
        distributeElements(groups, axis, dimension): void;
        removeShape(shape, hints): void;
        removeConnection(connection, hints): void;
        replaceShape(oldShape, newShape, hints): any;
        pasteElements(tree, topParent, position): void;
        alignElements(elements, alignment): void;
        resizeShape(shape, newBounds): void;
        createSpace(movingShapes, resizingShapes, delta, direction): any;
        updateWaypoints(connection, newWaypoints, hints): void;
        reconnectStart(connection, newSource, dockingOrPoints): void;
        reconnectEnd(connection, newTarget, dockingOrPoints): void;
        connect(source, target, attrs, hints): any;
        toggleCollapse(shape, hints): void;
        protected _create(type, attrs): any;
    }

    /*********************
     *  FEATURES / Rules *
     *********************/

    /** A service that provides rules for certain diagram actions. */
    class Rules {

        constructor(commandStack: any);

        /**
         * This method can be queried to ask whether certain modeling actions
         * are allowed or not.
         * @param  {String} action the action to be checked
         * @param  {Object} [context] the context to check the action in
         * @return {Boolean} returns true, false or null depending on whether the
         *                   operation is allowed, not allowed or should be ignored.
         */
        allowed(action: string, context?: any): boolean;

    }

    class RuleProvider extends CommandInterceptor {
        constructor(eventBus);
        addRule(actions, priority?: any, fn?: any);
        init(): any;
    }

    /************************
     *  FEATURES / Snapping *
     ************************/

    class Snapping {
        constructor(eventBus, canvas);
        addTargetSnaps(snapPoints, shape, target): void;
        getSiblings(element, target): any;
        getSnapLine(orientation): any;
        hide(): void;
        initSnap(event): any;
        showSnapLine(orientation, position): void;
        snap(event): any;
        protected _createLine(orientation): any;
        protected _createSnapLines(): void;
    }

    //////////////////////////////////////////////////////

    /************
     *  LAYOUT  *
     ************/

    class BaseLayouter {
        constructor();
        layoutConnection(connection, hints): any;
    }

    type Point = {
        x: number,
        y: number
    };

    type Bounds = {
        x: number,
        y: number,
        width: number,
        height: number
    }

    namespace ManhattanLayout {
        function getBendpoints(a: Point, b: Point, directions: string): Point[];
        function connectPoints(a: Point, b: Point, directions: string): Point[];
        function connectRectangles(source: Bounds, target: Bounds, start: Point, end: Point, hints: any): Point[];
        function repairConnection(source: Bounds, target: Bounds, start: Point, end: Point, waypoints: Point[], hints: any): Point[];
        function layoutStraight(source: Bounds, target: Bounds, start: Point, end: Point, hints: any): Point[];
        function getDirections(orientation: string, defaultLayout: string): string;
        function _repairConnectionSide(moved: Bounds, other: Bounds, newDocking: Point, points: Point[]);
    }

    namespace LayoutUtil {
        function roundBounds(bounds: Bounds): Bounds;
        function roundPoint(point: Point): Point;
        function asTRBL(bounds: Bounds): any;
        function asBounds(trbl: any): Bounds;
        function getMid(bounds: Bounds|Point): Point;
        function getOrientation(rect: Bounds, reference: Bounds, padding: Point|number): string;
        function getElementLineIntersection(elementPath, linePath, cropStart): Point;
        function getIntersections(a, b): Point[];
    }

    //////////////////////////////////////////////////////

    /***********
     *  MODEL  *
     ***********/

    namespace Model {

        abstract class Base {
            businessObject;
            parent;
            label;
            outgoing;
            incoming;
        }

        class Shape extends Base {
            children;
            host;
            attachers;
            constructor();
        }

        class Root extends Shape {
            constructor();
        }

        class Label extends Shape {
            constructor();
            labelTarget;
        }

        class Connection extends Base {
            source;
            target;
        }

    }

    //////////////////////////////////////////////////////

    /**********
     *  UTIL  *
     **********/

    namespace Collections {
        function remove(collection: any[], element: any): number;
        function add(collection: any[], element: any, idx: number): void;
        function indexOf(collection: any[], element: any): number;
    }

    namespace RenderUtil {
        function componentsToPath(elements: any): any;
        function toSVGPoints(points: Point[]): any;
        function createLine(points: Point[], attrs?: any): any;
        function updateLine(gfx: any, points: Point[]): any;
    }

    type size = {
        width?: number,
        height?: number
    }

    type textConfig = {
        align?: string,
        padding?: number,
        size?: size,
        style?: any
    }

    type textOptions = {
        align?: string,
        box?: any,
        padding?: number,
        style?: string
    }

    class Text {
        constructor(config: textConfig);
        createText(parent: SVGElement, text: string, options: textOptions): SVGTextElement;
    }

    //////////////////////////////////////////////////////

}

declare module 'diagram-js' {
    export = DiagramJS;
}

declare module 'diagram-js/lib/command/CommandInterceptor' {
    export = DiagramJS.CommandInterceptor;
}

declare module 'diagram-js/lib/core/Canvas' {
    export = DiagramJS.Canvas;
}

declare module 'diagram-js/lib/core/ElementFactory' {
    export = DiagramJS.ElementFactory;
}

declare module 'diagram-js/lib/draw/BaseRenderer' {
    export = DiagramJS.BaseRenderer;
}

declare module 'diagram-js/lib/features/connect/Connect' {
    export = DiagramJS.Connect;
}

declare module 'diagram-js/lib/features/context-pad/ContextPad' {
    export = DiagramJS.ContextPad;
}

declare module 'diagram-js/lib/features/create/Create' {
    export = DiagramJS.Create;
}

declare module 'diagram-js/lib/features/modeling/Modeling' {
    export = DiagramJS.Modeling;
}

declare module 'diagram-js/lib/features/rules/Rules' {
    export = DiagramJS.Rules;
}

declare module 'diagram-js/lib/features/rules/RuleProvider' {
    export = DiagramJS.RuleProvider;
}

declare module 'diagram-js/lib/features/snapping/Snapping' {
    export = DiagramJS.Snapping;
}

declare module 'diagram-js/lib/layout/BaseLayouter' {
    export = DiagramJS.BaseLayouter;
}

declare module 'diagram-js/lib/layout/LayoutUtil' {
    export = DiagramJS.LayoutUtil;
}

declare module 'diagram-js/lib/layout/ManhattanLayout' {
    export = DiagramJS.ManhattanLayout;
}

declare module 'diagram-js/lib/model/index' {
    export = DiagramJS.Model;
}

declare module 'diagram-js/lib/util/Collections' {
    export = DiagramJS.Collections;
}

declare module 'diagram-js/lib/util/RenderUtil' {
    export = DiagramJS.RenderUtil;
}

declare module 'diagram-js/lib/util/Text' {
    export = DiagramJS.Text;
}
