// Type definitions for bpmn-js 0.17.0
// Project: https://github.com/bpmn-io/bpmn-js
// Definitions by: Jan Steinbruecker <https://github.com/3fd>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference path='./diagram-js.d.ts'/>

declare namespace BpmnJS {

    class Viewer extends DiagramJS.Diagram {

        moddle: any;
        container: any;
        definitions: any;
        protected _modules: any[];
        protected _moddleExtensions: any;

        constructor(options: Options);

        public importXML(xml: string, done?: (err: any, warnings: any[]) => any): void;
        public saveXML(options: SaveXMLOptions, done?: (err: any, xml: string) => any): void;
        public saveSVG(options: any, done?: (err: any, svgStr: string) => any): void;
        public importDefinitions(definitions: any, done: () => any): void;
        public getModules(): string[];
        public destroy(): void;
        public on(event: string, priority: number, callback: (event: any) => any, target?: any): () => any;
        public off(event: string, callback: () => any): void;

        protected _init(container: any, moddle: any, options: any);
        protected _emit(type: string, event: any): any;
        protected _createContainer(options: any): any;
        protected _createModdle(options: any): any;

    }

    class Modeler extends Viewer {
        protected _interactionModules: any[];
        protected _modelingModules: any[];
        constructor(options: Options);
        public createDiagram(done?): any;
        protected _collectIds(definitions, context): any
    }

    class NavigatedViewer extends Viewer {
        protected _navigationModules: any[];
        constructor(options: Options);
    }

    class PathMap {
        pathMap: any;
        constructor();
        getRawPath(pathId: string);
        getScaledPath(pathId: string, param: any);
    }

    type Options = {
        container?: string,
        width?: string|number,
        height?: string|number,
        position?: string,
        propertiesPanel?: any,
        moddleExtensions?: any,
        modules?: any[],
        additionalModules?: any[]
    }

    type SaveXMLOptions = {
        format?: boolean
        preamble?: boolean
    }

    function checkValidationError(err: Error): Error;

    namespace ModelUtil {
        function is(element: any, type: string): boolean;
        function getBusinessObject(element: any): any;
    }

}

declare module 'bpmn-js' {
    export = BpmnJS;
}

declare module 'bpmn-js/lib/Viewer' {
    export = BpmnJS.Viewer;
}

declare module 'bpmn-js/lib/Modeler' {
    export = BpmnJS.Modeler;
}

declare module 'bpmn-js/lib/NavigatedViewer' {
    export = BpmnJS.NavigatedViewer;
}

declare module 'bpmn-js/lib/draw/PathMap' {
    export = BpmnJS.PathMap;
}

declare module 'bpmn-js/lib/util/ModelUtil' {
    export = BpmnJS.ModelUtil;
}
