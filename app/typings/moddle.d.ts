// Type definitions for moddle 0.8.0
// Project: https://github.com/bpmn-io/moddle
// Definitions by: Jan Steinbruecker <https://github.com/3fd>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare module 'moddle' {

    class Moddle {
        properties: Moddle.Properties;
        factory: Moddle.Factory;
        registry: Moddle.Registry;
        typeCache: Object;
        constructor(packages: Moddle.Package[]);
        create(descriptor: string|Moddle.Descriptor, attrs?: any): any;
        createAny(name: string, nsUri: string, properties: Moddle.Properties): any;
        getElementDescriptor(element: any): any;
        getPackage(uriOrPrefix: any): Moddle.Package;
        getPackages(): Moddle.Package[];
        getPropertyDescriptor(element: any, property: Moddle.Property): any;
        getType(descriptor: string|Moddle.Descriptor): Moddle.Type;
        hasType(element: any, type?: Moddle.Type): boolean;
    }

    namespace Moddle {

        interface Properties {
            model: Moddle;
            constructor(model: Moddle);
            set(target: any, name: string, value: any): void;
            get(target: any, name: string): any;
            define(target: any, name: string, options: any): void;
            defineDescriptor(target: any, descriptor: string): void;
            defineModel(target: any, model: any): void;
        }

        interface Factory {
            model: Moddle;
            properties: Properties;
            constructor(model: Moddle, properties: Properties);
            createType(descriptor: Descriptor): ModdleElement;
        }

        interface Ns {
            name: string,
            prefix: string,
            localName: string
        }

        interface Descriptor {
            ns: Ns;
            name: string
            allTypes: any[]
            properties: any[]
            propertiesByName: Object
        }

        interface Base {
            get(name: string): any;
            set(name: string , value): void;
        }

        interface ModdleElement extends Base {

        }

        type Property = {
            name: string,
            type: string,
            redefines?: string
            isMany?: boolean,
            isReference?: boolean,
            isAttr?: boolean,
            isBody?: boolean,
            serialize?: string
        }

        interface Type {
            name: string,
            properties: Property[],
            superClass?: string[]
        }

        interface Package {
            name: string,
            types: Type[],

            prefix: string,
            uri?: string
            enumerations?: any[],
            associations?: any[],
            xml?: any,
        }

        interface Registry {
            packageMap: Object;
            typeMap: Object;
            packages: Package[];
            properties: Properties;
            constructor(packages: Package[], properties: Properties);
            getPackage(uriOrPrefix: string): Package;
            getPackages(): Package[];
            registerPackage(pkg: Package): void;
            registerType(type: Type, pkg: Package): void;
            mapTypes(nsName: any, iterator: any, trait: boolean): void;
            getEffectiveDescriptor(name: string): Descriptor;
            definePackage(target: any, pkg: Package): void;
        }

    }

    export = Moddle;
}

