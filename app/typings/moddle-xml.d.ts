// Type definitions for moddle-xml 0.12.0
// Project: https://github.com/bpmn-io/moddle-xml
// Definitions by: Jan Steinbruecker <https://github.com/3fd>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare namespace ModdleXML {

    class ElementHandler {
        constructor(model: any, type: string, context?: any)
        addReference(reference: any): void;
        createElement(node: any): any;
        getPropertyForNode(node: any): any
        handleChild(node: any): any;
        handleEnd(): void;
        handler(type: string): any;
        referenceHandler(propertyDesc: string): any;
        toString(): string
        valueHandler(propertyDesc: string, element: any): any;
    }

    class Reader {
        constructor(options?: any);
        fromXML(xml: string, options: any, done: () => any): void;
        handler(name: string): ElementHandler;
    }

    class Writer {
        options: any;
        constructor(options?: { format?: boolean, preamble?: boolean });
        toXML(tree: any, writer?: any): any;
    }

}

declare module "moddle-xml" {
    export = ModdleXML;
}

declare module "moddle-xml/lib/reader" {
    export = ModdleXML.Reader;
}

declare module "moddle-xml/lib/writer" {
    export = ModdleXML.Writer;
}

/*

 import { Reader as XMLReader, Writer as XMLWriter } from 'moddle-xml';

 or

 import { Reader, Writer} from 'moddle-xml';

 or

 import * as ModdleXML from 'moddle-xml';

 or

 import XMLReader = require('moddle-xml/lib/reader');
 import XMLWriter = require('moddle-xml/lib/writer');

 */
