//---------------------IMPORTS---------------------
import UmlRenderer from './UmlRenderer';
import PathMap from './PathMap';


//---------------------EXPORTS---------------------
module.exports = {

    __init__: [ 'umlRenderer' ],
    umlRenderer: [ 'type', UmlRenderer ],
    pathMap: [ 'type', PathMap ]

};
