//---------------------IMPORTS---------------------
import BpmnPathMap = require('bpmn-js/lib/draw/PathMap');


//---------------------CLASS---------------------
export default class PathMap extends BpmnPathMap {


    //---------------------CONSTRUCTOR---------------------
    constructor() {

        super();
        this.pathMap = {
            'ANCHOR': {
                d: 'm {mx},{my} c 22.88913841,0.3672 28.52200841,8.0773 28.52200841,8.0773 0,0 -5.083253,7.5059 -28.52200841,7.8937 12.66782741,-3.0425 15.52145341,-7.768 15.52145341,-7.768 0,0 -1.763478,-5.1736 -15.52145341,-8.203 z',
                height: 18,
                width: 30
            }
        };
    }
}
