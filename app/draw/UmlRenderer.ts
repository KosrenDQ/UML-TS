//---------------------IMPORTS---------------------
import { assign, isObject } from 'lodash';
import { is } from '../util/ModelUtil';
import { createLine } from 'diagram-js/lib/util/RenderUtil';
import BaseRenderer = require('diagram-js/lib/draw/BaseRenderer');
import TextUtil = require('diagram-js/lib/util/Text');

import domQuery = require('min-dom/lib/query');

import svgAppend = require('tiny-svg/lib/append');
import svgAttr = require('tiny-svg/lib/attr');
import svgCreate = require('tiny-svg/lib/create');
import svgClasses = require('tiny-svg/lib/classes');


//---------------------CONSTANTS---------------------
const LABEL_STYLE = {

    fontFamily: 'Arial, sans-serif',
    fontSize: '12px'

};

//---------------------CLASS---------------------
export default class UmlRenderer extends BaseRenderer {


    //---------------------ATTRIBUTES---------------------
    markers = {};
    computeStyle: any;
    textUtil: any;
    handlers: any;
    canvas: any;


    //---------------------CONSTRUCTOR---------------------
    constructor(eventBus, styles, pathMap, canvas, priority) {
        super(eventBus, priority);

        this.textUtil = new TextUtil({
            style: LABEL_STYLE,
            size: { width: 100 }
        });

        this.canvas = canvas;
        this.computeStyle = styles.computeStyle;

        const CONNECTION_STYLE = styles.style([ 'no-fill '], { strokeWidth: 2, stroke: 'slategray' });
        const DASHED_CONNECTION_STYLE = styles.style([ 'no-fill '], { strokeWidth: 2, stroke: 'slategray', strokeDasharray: [5, 10] });
        const CLASS_STYLE = styles.style({ fill: 'Transparent', stroke: 'slategray', strokeWidth: 2 });

        this.initMarker();

        const self = this;
        this.handlers = {

            'uml:Class': function(visuals, element) {

                const radius = 4;
                const ret = self.drawRect(visuals, element.width || 0, element.height || 0, radius, CLASS_STYLE);
                self.drawLine(visuals, [
                    { x: 0, y: 30},
                    { x: element.width, y: 30}
                ]);
                self.renderEmbeddedLabel(visuals, element, 'center-top');

                if(element.umlElement.isAbstract) {

                    self.renderStereotype(visuals, element, 'abstract', '');

                } else if(element.umlElement.isInterface) {

                    self.renderStereotype(visuals, element, 'interface', '');

                }

                let attribute = '';

                for(let attr of element.umlElement.get('ownedProperty')) {

                    if(attr.visibility === 'Private') {

                        attribute += '-';

                    } else if(attr.visibility === 'Public') {

                        attribute += '+';

                    } else if(attr.visibility === 'Protected') {

                        attribute += '#';

                    }

                    attribute += ` ${attr.name}: ${attr.type}\n`;

                }

                let method = '';

                for(let met of element.umlElement.get('ownedOperation')) {

                    if(met.visibility === 'Private') {

                        method += '-';

                    } else if(met.visibility === 'Public') {

                        method += '+';

                    } else if(met.visibility === 'Protected') {

                        method += '#';

                    }

                    method += ` ${met.name}(`;

                    for(let prop of met.get('parameter')) {

                        method += `${prop.name}: ${prop.type}`;

                        if(met.get('parameter').indexOf(prop) < met.get('parameter').length - 1) {

                            method += ', ';

                        }
                    }

                    method += `): ${met.returnType}\n`;

                }

                const y = element.umlElement.get('ownedProperty').length * 14 + 33;
                self.drawLine(visuals, [
                    { x: 0, y: y},
                    { x: element.width, y: y}
                ]);

                self.renderAttrMethodLabel(visuals, attribute + method, 'left-top', element);

                return ret;

            },
            'uml:Enum': function(visuals, element) {

                const radius = 4;
                const ret = self.drawRect(visuals, element.width || 0, element.height || 0, radius, CLASS_STYLE);
                self.drawLine(visuals, [
                    { x: 0, y: 30},
                    { x: element.width, y: 30}
                ]);
                self.renderEmbeddedLabel(visuals, element, 'center-top');
                self.renderStereotype(visuals, element, 'enumeration', '');
                
                let literal = ``;
                
                for(let lit of element.umlElement.get('ownedLiteral')) {
                    
                    literal += `${lit.name.toUpperCase()}\n`;
                    
                }
                
                self.renderAttrMethodLabel(visuals, literal, 'left-top', element);
                
                return ret;
                
                
            },
            'uml:Association': function(visual, element) {

                const attrs = CONNECTION_STYLE;
                attrs.markerEnd = {};
                return self.drawLine(visual, element.waypoints, attrs);
                
            },
            'uml:SuperClass': function(visual, element) {

                let attrs = CONNECTION_STYLE;
                attrs.markerEnd = self.getMarker('connection-end-none');

                if(element.umlElement.fatherClass && element.umlElement.fatherClass.isInterface) {

                    attrs = DASHED_CONNECTION_STYLE;
                    attrs.markerEnd = self.getMarker('connection-end-none');

                }
                
                return self.drawLine(visual, element.waypoints, attrs);

            },
            'uml:Relation': function(visual, element) {

                const attrs = DASHED_CONNECTION_STYLE;
                attrs.markerEnd = {};
                return self.drawLine(visual, element.waypoints, attrs);
                
            },
            'label': function(visual, element) {

                if(element.assocDock === 'sourceName') {

                    return self.renderExternalLabel(visual, element, 'left', element.umlElement.sourceName);
                    
                } else if(element.assocDock === 'sourceCard') {

                    return self.renderExternalLabel(visual, element, 'left', element.umlElement.sourceCard);

                } else if(element.assocDock === 'targetName') {

                    return self.renderExternalLabel(visual, element, 'right', element.umlElement.targetName);

                } else if(element.assocDock === 'targetCard') {

                    return self.renderExternalLabel(visual, element, 'right', element.umlElement.targetCard);

                }
            }
        };
    }


    //---------------------METHODS---------------------
    addMarker(id, element) {

        this.markers[id] = element;

    }

    getMarker(id) {

        return 'url(#' + id + ')';

    }

    initMarker() {

        const self = this;
        function createMarker(id, options) {

            const attrs = assign({

                fill: 'black',
                strokeWidth: 1

            }, options.attrs);

            const ref = options.ref || { x: 0, y: 0 };
            const scale = options.scale || 1;

            if(attrs.strokeDasharray === 'none') {

                attrs.strokeDasharray = [10000, 1];

            }

            const marker = svgCreate('marker');
            svgAttr(options.element, attrs);
            svgAppend(marker, options.element);
            
            svgAttr(marker, {
                id: id,
                viewBox: '0 0 20 20',
                refX: ref.x,
                refY: ref.y,
                markerWidth: 20 * scale,
                markerHeight: 20 * scale,
                orient: 'auto'
            });

            let defs = domQuery('defs', self.canvas._svg);
            
            if(!defs) {
                
                defs = svgCreate('defs');
                svgAppend(self.canvas._svg, defs);
                
            }
            
            svgAppend(defs, marker);
            self.addMarker(id, marker);

        }

        const path1 = svgCreate('path');
        svgAttr(path1, { d: 'M 1 5 L 11 10 L 1 15 M 1 15 L 1 5' });
        createMarker('connection-end-none', {
            element: path1,
            attrs: {
                fill: 'white',
                stroke: 'slategray',
                strokeWidth: 1
            },
            ref: { x: 12, y: 10 },
            scale: 1
        });

        const path2 = svgCreate('path');
        svgAttr(path2, { d: 'M 1 5 L 11 10 L 1 15 M 1 15 L 1 5' });
        createMarker('connection-end-dashed', {
            element: path2,
            attrs: {
                fill: 'white',
                stroke: 'slategray',
                strokeWidth: 1,
                strokeDasharray: [2, 4]
            },
            ref: { x: 12, y: 10 },
            scale: 1
        });
    }

    drawRect(visuals, width, height, r, offset, attrs?) {

        if(isObject(offset)) {

            attrs = offset;
            offset = 0;

        }

        offset = offset || 0;

        attrs = this.computeStyle(attrs, {
            stroke: 'black',
            strokeWidth: 2,
            fill: 'white'
        });
        
        const rect = svgCreate('rect');
        svgAttr(rect, {
            x: offset,
            y: offset,
            width: width - offset * 2,
            height: height - offset * 2,
            rx: r,
            ry: r
        });
        svgAttr(rect, attrs);
        svgAppend(visuals, rect);

        return rect;

    }

    drawLine(p, waypoints, attrs?) {

        attrs = this.computeStyle(attrs, [ 'no-fill' ], {
            stroke: 'slategray',
            strokeWidth: 2,
            fill: 'white'
        });
        
        const line = createLine(waypoints, attrs);
        svgAppend(p, line);
        
        return line;

    }

    renderLabel(p, label, options) {

        const text = this.textUtil.createText(p, label || '', options);
        svgClasses(text).add('djs-label');
        
        return text;

    }

    renderStereotype(p, element, type, align) {

        this.renderLabel(p, '<<' + type + '>>', { box: element, align: align, style: { transform: 'translate(0, -25)', fontSize: '14px' }});

    }

    renderExternalLabel(p, element, align, text) {

        return this.renderLabel(p, text, { box: element, align: align, style: { fontSize: '11px' }});

    }

    renderEmbeddedLabel(p, element, align) {

        return this.renderLabel(p, element.umlElement.name, { box: element, align: align, padding: 5 });

    }
    
    renderAttrMethodLabel(p, attribute, align, element) {

        return this.renderLabel(p, attribute, { box: element, align: align, padding: { left: 5, right: 5, bottom: 5, top: 30 }});

    }

    canRender(element): boolean {

        return is(element, 'uml:Element') || is(element, 'label');

    }

    drawShape(visuals, element) {

        const handler = this.handlers[element.type];
        return handler(visuals, element);

    }

    drawConnection(visuals, element) {

        const handler = this.handlers[element.type];
        return handler(visuals, element);

    }
}

(UmlRenderer as any).$inject = [ 'eventBus', 'styles', 'pathMap', 'canvas' ];
