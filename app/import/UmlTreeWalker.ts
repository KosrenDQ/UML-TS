//---------------------IMPORTS---------------------
import * as Refs from 'object-refs';


//---------------------CONSTANTS---------------------
const diRefs = new Refs({ name: 'umlElement', enumerable: true }, { name: 'di' });


//---------------------CLASS---------------------
export default class UmlTreeWalker {


    //---------------------ATTRIBUTES---------------------
    private baseElements = [];


    //---------------------CONSTRUCTOR---------------------
    constructor(private handler: any) { }


    //---------------------METHODS---------------------
    ///// Helpers /////////////////////////////////
    visit(element, context) {

        return this.handler.element(element, context);

    }

    visitClassGraph(element) {

        return this.handler.root(element);

    }

    visitIfDi(element, context) {

        try {

            return element.di && this.visit(element, context);

        } catch(e) {

            this.logError(e.message, { element: element, error: e});
            console.error(`failed to import ${this.elementToString(element)}`);
            console.error(e);

        }
    }

    logError(message, context) {

        console.log(message, context);
        this.handler.error(message, context);

    }

    contextual(fn, ctx?) {

        return function(e) {

            fn(e, ctx);

        };
    }

    elementToString(element) {

        if (!element) {

            return '<null>';

        }

        return `<${element.$type} ${element.id ? ' id="' + element.id : ''} />`;

    }

    ////// DI handling ////////////////////////////
    registerDi(di) {

        const umlElement = di.umlElement;

        if (umlElement) {

            if (umlElement.di) {

                this.logError(`multiple DI elements defined for ${this.elementToString(umlElement)}`, { element: umlElement });

            }
            else {

                this.baseElements.push(umlElement);
                diRefs.bind(umlElement, 'di');
                umlElement.di = di;

            }
        }
        else {

            this.logError(`no umlElement referenced in ${this.elementToString(di)}`, { element: di });

        }
    }

    handleDiagram(diagram) {

        this.handleDiagramElements(diagram.diagramElements);

    }

    handleDiagramElements(diagramElements) {

        if(diagramElements) {

            for(let element of diagramElements) {

                this.handleDiagramElement(element);

            }
        }
    }

    handleDiagramElement(diagramElement) {

        this.registerDi(diagramElement);

        if(diagramElement.$instanceOf('umldi:UMLPlane') && diagramElement.planeElements) {

            for(let element of diagramElement.planeElements) {

                this.handleDiagramElement(element);

            }
        }
    }

    ////// Semantic handling //////////////////////
    handleDefinitions(definitions, diagram?: any) {

        const diagrams = definitions.diagram;

        if(diagram && diagrams) {

            throw new Error('diagram not part of uml:ClassGraph');

        }

        if(!diagram && diagrams) {

            diagram = diagrams;

        }

        if(!diagram) {

            throw new Error('no diagram to display');

        }

        this.handleDiagram(diagram);
        const context = this.visitClassGraph(diagram);

        for(let element of this.baseElements) {

            this.contextual(this.handleBaseElement(element, context), context);

        }
    }

    handleBaseElement(element, context?) {

        if (element.$instanceOf('uml:Class')) {

            this.handleClass(element, context);

        } else if (element.$instanceOf('uml:Enum')) {

            this.handleEnum(element, context);

        } else if (element.$instanceOf('uml:Association')) {

            this.handleAssociation(element, context);

        } else if (element.$instanceOf('uml:SuperClass')) {

            this.handleSuperClass(element, context);

        } else if (element.$instanceOf('uml:Relation')) {
            
            this.handleRelation(element, context);
            
        }
    }

    handleClass(umlClass, ctx) {

        this.visitIfDi(umlClass, ctx);

    }
    
    handleEnum(umlEnum, ctx) {
        
        this.visitIfDi(umlEnum, ctx);
        
    }

    handleAssociation(association, ctx) {

        this.visitIfDi(association, ctx);

    }

    handleSuperClass(superClass, ctx) {

        this.visitIfDi(superClass, ctx);

    }
    
    handleRelation(relation, ctx) {
        
        this.visitIfDi(relation, ctx);
        
    }
}
