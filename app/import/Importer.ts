//---------------------IMPORTS---------------------
import UmlTreeWalker from './UmlTreeWalker';


//---------------------CLASS---------------------
class Visitor {


    //---------------------ATTRIBUTES---------------------
    importer: any;
    warnings = [];


    //---------------------CONSTRUCTOR---------------------
    constructor(importer: any) {

        this.importer = importer;

    }


    //---------------------METHODS---------------------
    root(element) {

        return this.importer.root(element);

    }

    element(element, parentShape) {

        return this.importer.add(element, parentShape);

    }

    error(message, context) {

        this.warnings.push({

            message: message,
            context: context

        });
    }
}


//---------------------API---------------------
export function importUmlDiagram(diagram, definitions, done) {

    const importer = diagram.get('umlImporter');
    const eventBus = diagram.get('eventBus');

    let error;
    let warnings = [];

    function render(definitions) {

        const visitor = new Visitor(importer);
        const walker = new UmlTreeWalker(visitor);
        walker.handleDefinitions(definitions);

    }

    eventBus.fire('import.render.start', {

        definitions: definitions

    });

    try {

        render(definitions);

    } catch(e) {

        error = e;

    }

    eventBus.fire('import.render.complete', {

        error: error,
        warnings: warnings

    });

    done(error, warnings);

}
