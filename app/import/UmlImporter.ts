//---------------------IMPORTS---------------------
import { assign, map } from 'lodash';
import * as LabelUtil from '../features/label-editing/LabelUtil';

function elementData(semantic, attrs?) {

    return assign({
        id: semantic.id,
        type: semantic.$type,
        umlElement: semantic
    }, attrs);
}

type Point = {

    x: number|string,
    y: number|string

}

function collectWaypoints(waypoints) {

    return map(waypoints, function(p: Point) {
        return { x: p.x, y: p.y };
    });
}

//---------------------CLASS---------------------
export default class UmlImporter {


    //---------------------CONSTRUCTOR---------------------
    constructor(private eventBus, private canvas, private elementFactory, private elementRegistry) { }


    //---------------------METHODS---------------------
    root(diagram) {

        const element = this.elementFactory.createRoot(elementData(diagram));
        
        if(this.canvas.getRootElement()) {
            
            this.canvas._clear();
            
        }
        
        this.canvas.setRootElement(element);
        return element;

    }

    add(umlElement, parentElement) {

        const di = umlElement.di;
        let element;

        if (di && di.$instanceOf('umldi:UMLShape')) {

            const bounds = umlElement.di.bounds;

            element = this.elementFactory.createShape(elementData(umlElement, {

                x: Math.round(bounds.x),
                y: Math.round(bounds.y),
                width: Math.round(bounds.width),
                height: Math.round(bounds.height)

            }));

            this.canvas.addShape(element, parentElement);

        } else if (di && di.$instanceOf('umldi:UMLEdge')) {

            const source = this.getSource(umlElement);
            const target = this.getTarget(umlElement);

            element = this.elementFactory.createConnection(elementData(umlElement, {

                source: source,
                target: target,
                waypoints: collectWaypoints(umlElement.di.waypoint)

            }));

            this.canvas.addConnection(element, parentElement);

        } else {

            throw new Error('unknown di');

        }

        if (LabelUtil.hasExternalLabel(umlElement)) {

            this.addLabels(umlElement, element);

        }

        this.eventBus.fire('umlElement.added', {element: element});
        return element;

    }

    addLabels(semantic, element) {

        const bounds = LabelUtil.getAssocSourceDockTop(element.waypoints);
        const label = this.elementFactory.createLabel(elementData(semantic, {
            id: semantic.id + '_label_sourceName',
            labelTarget: element,
            type: 'label',
            x: Math.round(bounds.x) - (LabelUtil.DEFAULT_LABEL_SIZE.width / 2),
            y: Math.round(bounds.y) - (LabelUtil.DEFAULT_LABEL_SIZE.height / 2),
            width: Math.round(LabelUtil.DEFAULT_LABEL_SIZE.width),
            height: Math.round(LabelUtil.DEFAULT_LABEL_SIZE.height),
            assocDock: 'sourceName'
        }));
        this.canvas.addShape(label, element.parent);

        const bounds2 = LabelUtil.getAssocSourceDockBottom(element.waypoints);
        const label2 = this.elementFactory.createLabel(elementData(semantic, {
            id: semantic.id + '_label_sourceCard',
            labelTarget: element,
            type: 'label',
            x: Math.round(bounds2.x) - (LabelUtil.DEFAULT_LABEL_SIZE.width / 2),
            y: Math.round(bounds2.y) - (LabelUtil.DEFAULT_LABEL_SIZE.height / 2),
            width: Math.round(LabelUtil.DEFAULT_LABEL_SIZE.width),
            height: Math.round(LabelUtil.DEFAULT_LABEL_SIZE.height),
            assocDock: 'sourceCard'
        }));
        this.canvas.addShape(label2, element.parent);

        const bounds3 = LabelUtil.getAssocTargetDockTop(element.waypoints);
        const label3 = this.elementFactory.createLabel(elementData(semantic, {
            id: semantic.id + '_label_targetName',
            labelTarget: element,
            type: 'label',
            x: Math.round(bounds3.x) - (LabelUtil.DEFAULT_LABEL_SIZE.width / 2),
            y: Math.round(bounds3.y) - (LabelUtil.DEFAULT_LABEL_SIZE.height / 2),
            width: Math.round(LabelUtil.DEFAULT_LABEL_SIZE.width),
            height: Math.round(LabelUtil.DEFAULT_LABEL_SIZE.height),
            assocDock: 'targetName'
        }));
        this.canvas.addShape(label3, element.parent);

        const bounds4 = LabelUtil.getAssocTargetDockBottom(element.waypoints);
        const label4 = this.elementFactory.createLabel(elementData(semantic, {
            id: semantic.id + '_label_targetCard',
            labelTarget: element,
            type: 'label',
            x: Math.round(bounds4.x) - (LabelUtil.DEFAULT_LABEL_SIZE.width / 2),
            y: Math.round(bounds4.y) - (LabelUtil.DEFAULT_LABEL_SIZE.height / 2),
            width: Math.round(LabelUtil.DEFAULT_LABEL_SIZE.width),
            height: Math.round(LabelUtil.DEFAULT_LABEL_SIZE.height),
            assocDock: 'targetCard'
        }));
        this.canvas.addShape(label4, element.parent);
        
    }

    getSource(association) {

        if (association.$instanceOf('uml:Association')) {

            const sourceRes = association.sourceClass;
            return this.elementRegistry.get(sourceRes.id);

        } else if (association.$instanceOf('uml:SuperClass')) {

            const childRes = association.childClass;
            return this.elementRegistry.get(childRes.id);

        } else if (association.$instanceOf('uml:Relation')) {
            
            const parentRes = association.parentClass;
            return this.elementRegistry.get(parentRes.id);
            
        }
    }

    getTarget(association) {

        if (association.$instanceOf('uml:Association')) {

            const targetRes = association.targetClass;
            return this.elementRegistry.get(targetRes.id);

        } else if (association.$instanceOf('uml:SuperClass')) {

            const fatherRes = association.fatherClass;
            return this.elementRegistry.get(fatherRes.id);

        } else if (association.$instanceOf('uml:Relation')) {
            
            const childEnumRes = association.childEnum;
            return this.elementRegistry.get(childEnumRes.id);
            
        }
    }
}
