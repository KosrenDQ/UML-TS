//---------------------IMPORTS---------------------
import UmlImporter from './UmlImporter';


//---------------------EXPORTS---------------------
module.exports = {

    umlImporter: [ 'type', UmlImporter ]

};
