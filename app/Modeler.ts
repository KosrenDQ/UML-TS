//---------------------IMPORTS---------------------
import Ids = require('ids');
import Viewer = require('bpmn-js/lib/Viewer');
import { assign } from 'lodash';

import UmlModdle from './UmlModdle';
import { importUmlDiagram } from './import/Importer';
import { Options, checkValidationError } from 'bpmn-js';


//---------------------CONSTANTS---------------------
const DEFAULT_PRIORITY = 1000;
const DEFAULT_OPTIONS = {

    width: '100%',
    height: '100%',
    position: 'relative',
    container: 'body'

};
const DEFAULT_MODULES = [
    // modules the viewer is composed of
    require('diagram-js/lib/features/overlays'),
    require('diagram-js/lib/features/selection'),
    // non-modeling components
    require('diagram-js/lib/navigation/movecanvas'),
    require('diagram-js/lib/navigation/touch'),
    require('diagram-js/lib/navigation/zoomscroll'),
    // modeling components
    require('diagram-js/lib/features/auto-scroll'),
    require('diagram-js/lib/features/bendpoints'),
    require('diagram-js/lib/features/hand-tool'),
    require('diagram-js/lib/features/lasso-tool'),
    require('diagram-js/lib/features/move'),
    require('diagram-js/lib/features/resize'),
    require('diagram-js/lib/features/space-tool'),
    require('./draw'),
    require('./import'),
    require('./layout'),
    require('./features/context-pad'),
    require('./features/label-editing'),
    require('./features/modeling'),
    require('./features/palette'),
    require('./features/properties-panel'),
    require('./features/rules'),
    require('./features/snapping'),
];


//---------------------CLASS--------------------
export class Modeler extends Viewer {


    //---------------------CONSTRUCTOR---------------------
    constructor(options?: Options) {

        options = assign({}, DEFAULT_OPTIONS, options);
        super(options);
        super.destroy();
        
        this.moddle = this._createModdle(options);
        this.container = this._createContainer(options);
        this._init(this.container, this.moddle, options);
        
        this.on('import.parse.complete', DEFAULT_PRIORITY, function(event) {

            if(!event.error) {

                this._collectIds(event.definitions, event.context);

            }

        }, this);

        this.on('diagram.destroy', DEFAULT_PRIORITY, function() {

            this.moddle.ids.clear();

        }, this);
    }


    //---------------------METHODS---------------------
    importXML(xml, done: (err, warnings?) => void = function() {}) {

        const self = this;
        xml = this._emit('import.parse.start', { xml: xml }) || xml;

        this.moddle.fromXML(xml, 'uml:ClassGraph', {}, function(err, definitions, context) {

            definitions = self._emit('import.parse.complete', {

                    error: err,
                    definitions: definitions,
                    context: context

            }) || definitions;

            if(err) {

                err = checkValidationError(err);
                self._emit('import.done', { error: err });
                return done(err);

            }

            const parseWarnings = context.warnings;

            self.importDefinitions(definitions, function(err, importWarnings) {

                const allWarnings = [].concat(parseWarnings, importWarnings || []);
                self._emit('import.done', { error: err, warnings: allWarnings });
                done(err, allWarnings);

            });
        });
    }
    
    importJSON(json, done: (err, warnings?) => void = function() {}) {

        const self = this;
        this.moddle.fromJSONToXML(json, function(err, xml) {
            
            if(!err) {

                self.importXML(xml, function(err) {
                    
                    done(err);
                    
                });

            } else {
           
                done(err);
                
            }
        });
    }
    
    importDefinitions(definitions, done) {

        try {

            if(this.definitions) {

                this.clear();

            }

            this.definitions = definitions;
            importUmlDiagram(this, definitions, done);

        } catch(e) {

            done(e);

        }
    }

    exportJson(done) {

        this.moddle.toJSON(this.definitions, done);
    
    }

    protected _createModdle(options) {

        this._modules = DEFAULT_MODULES;
        const moddle = new UmlModdle(options.moddleExtensions);
        moddle.ids = new Ids([ 32, 36, 1 ]);
        return moddle;

    }

    protected _collectIds(definitions, context) {

        const moddle = definitions.$model;
        const ids: Ids = moddle.ids;
        ids.clear();

        for(let id of context.elementsById) {

            ids.claim(id, context.elementsById[id]);

        }
    }
}
