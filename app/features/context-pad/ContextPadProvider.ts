//---------------------IMPORTS---------------------
import Canvas = require('diagram-js/lib/core/Canvas');
import Connect = require('diagram-js/lib/features/connect/Connect');
import ContextPad = require('diagram-js/lib/features/context-pad/ContextPad');
import Create = require('diagram-js/lib/features/create/Create');
import Rules = require('diagram-js/lib/features/rules/Rules');
import { assign } from 'lodash';

import ElementFactory from '../modeling/ElementFactory';
import Modeling from '../modeling/Modeling';


//---------------------CLASS---------------------
export default class ContextPadProvider {


    //---------------------CONSTRUCTOR---------------------
    constructor(private contextPad: ContextPad,
                private modeling: Modeling,
                private elementFactory: ElementFactory,
                private connect: Connect,
                private create: Create,
                private canvas: Canvas,
                private rules: Rules) {

        contextPad.registerProvider(this);

    }


    //---------------------METHODS---------------------
    getContextPadEntries(element) {

        let actions = {};
        const umlElement = element.umlElement;

        const addAttribute = (event) => {
            this.modeling.addClassAttribute(element);
        };

        const addMethod = (event) => {
            this.modeling.addClassMethod(element);
        };
        
        const addLiteral = (event) => {
            this.modeling.addEnumLiteral(element);  
        };

        const startConnect = (event, element, autoActivate) => {
            element.superClass = false;
            element.enumLink = false;
            this.connect.start(event, element, autoActivate);
        };

        const startSuperClass = (event, element, autoActivate) => {
            element.superClass = true;
            element.enumLink = false;
            this.connect.start(event, element, autoActivate);
        };
        
        const startLinkToEnum = (event, element, autoActivate) => {
            element.superClass = false;
            element.enumLink = true;
            this.connect.start(event, element, autoActivate);
        };

        const removeElement = (event) => {
            this.modeling.removeElements([element]);
        };

        if (umlElement) {
            if (umlElement.$instanceOf('uml:Class')) {
                assign(actions, {
                    'connect': {
                        group: 'connect',
                        className: 'bpmn-icon-connection',
                        title: 'Create Association',
                        action: {
                            click: startConnect,
                            dragstart: startConnect
                        }
                    },
                    'inheritance': {
                        group: 'connect',
                        className: 'bpmn-icon-connection',
                        title: 'Create Inheritance',
                        action: {
                            click: startSuperClass,
                            dragstart: startSuperClass
                        }
                    },
                    'linkToEnum': {
                        group: 'connect',
                        className: 'bpmn-icon-conditional-flow',
                        title: 'Create Link to Enumeration',
                        action: {
                            click: startLinkToEnum,
                            dragstart: startLinkToEnum
                        }
                    },
                    'addAttr': {
                        group: 'edit',
                        className: 'bpmn-icon-data-input',
                        title: 'Add attribute',
                        action: {
                            click: addAttribute,
                            dragstart: addAttribute
                        }
                    },
                    'addMethod': {
                        group: 'edit',
                        className: 'bpmn-icon-sub-process-marker',
                        title: 'Add method',
                        action: {
                            click: addMethod,
                            dragstart: addMethod
                        }
                    },
                    'delete': {
                        group: 'edit',
                        className: 'bpmn-icon-trash',
                        title: 'Remove',
                        action: {
                            click: removeElement,
                            dragstart: removeElement
                        }
                    }
                });

            } else if (umlElement.$instanceOf('uml:Enum')) {
                
                assign(actions, {
                    'addLiteral': {
                        group: 'edit',
                        className: 'bpmn-icon-data-input',
                        title: 'Add Literal',
                        action: {
                            click: addLiteral,
                            dragstart: addLiteral
                        }
                    }, 
                    'delete': {
                        group: 'edit',
                        className: 'bpmn-icon-trash',
                        title: 'Remove',
                        action: {
                            click: removeElement,
                            dragstart: removeElement
                        }
                    }
                });
                
            } else if ((umlElement.$instanceOf('uml:Association') || umlElement.$instanceOf('uml:SuperClass') || umlElement.$instanceOf('uml:Relation')) && element.type !== 'label') {

                assign(actions, {
                    'delete': {
                        group: 'edit',
                        className: 'bpmn-icon-trash',
                        title: 'Remove',
                        action: {
                            click: removeElement,
                            dragstart: removeElement
                        }
                    }
                });
            }
        }

        return actions;

    }
}

(ContextPadProvider as any).$inject = [ 'contextPad', 'modeling', 'elementFactory', 'connect', 'create', 'canvas', 'rules' ];
