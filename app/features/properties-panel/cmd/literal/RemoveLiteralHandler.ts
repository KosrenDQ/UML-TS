//---------------------CLASS---------------------
export default class RemoveLiteralHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const oldValue = context.element.literal;
        const index = context.element.elem.umlElement.get('ownedLiteral').indexOf(oldValue);

        context.element.elem.umlElement.get('ownedLiteral').splice(index, 1);
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.elem.umlElement.get('ownedLiteral').push(context.oldProperties);
        return context.changed;

    }
}
