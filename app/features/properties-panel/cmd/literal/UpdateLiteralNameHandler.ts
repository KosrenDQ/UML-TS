//---------------------CLASS---------------------
export default class UpdateLiteralNameHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newName;
        const oldValue = context.element.literal.name;

        context.element.literal.name = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.literal.name = context.oldProperties;
        return context.changed;

    }
}
