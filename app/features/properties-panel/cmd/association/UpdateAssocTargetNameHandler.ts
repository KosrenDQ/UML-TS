//---------------------CLASS---------------------
export default class UpdateAssocTargetNameHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newName;
        const oldValue = context.element.assoc.targetName;

        context.element.assoc.targetName = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.assoc.targetName = context.oldProperties;
        return context.changed;

    }
}
