//---------------------CLASS---------------------
export default class UpdateAssocSourceNameHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newName;
        const oldValue = context.element.assoc.sourceName;

        context.element.assoc.sourceName = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.assoc.sourceName = context.oldProperties;
        return context.changed;

    }
}
