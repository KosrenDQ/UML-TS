//---------------------CLASS---------------------
export default class UpdateAssocSourceCardHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newCard;
        const oldValue = context.element.assoc.sourceCard;

        context.element.assoc.sourceCard = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.assoc.sourceCard = context.oldProperties;
        return context.changed;

    }
}
