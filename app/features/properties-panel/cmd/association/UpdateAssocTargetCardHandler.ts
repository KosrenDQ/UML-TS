//---------------------CLASS---------------------
export default class UpdateAssocTargetCardHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newCard;
        const oldValue = context.element.assoc.targetCard;

        context.element.assoc.targetCard = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.assoc.targetCard = context.oldProperties;
        return context.changed;

    }
}
