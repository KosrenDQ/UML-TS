//---------------------IMPORTS---------------------
import { forEach, keys, reduce } from 'lodash';


//---------------------CONSTANTS---------------------
const DEFAULT_FLOW = 'default';
const NAME = 'name';
const ID = 'id';


//---------------------GLOBAL_METHODS---------------------
function getProperties(umlElement, propertyNames) {

    return reduce(propertyNames, function(result: any, key: any) {

        result[key] = umlElement.get(key);
        return result;

    }, {});
}

function setProperties(umlElement, properties) {

    forEach(properties, function(value, key) {

        umlElement.set(key, value);

    });
}


//---------------------CLASS---------------------
export default class UpdatePropertiesHandler {


    //---------------------CONSTRUCTOR---------------------
    constructor(private elementRegistry, private moddle) { }


    //---------------------METHODS---------------------
    execute(context) {

        const element = context.element;
        const changed = [element];

        if(!element) {
            throw new Error('element required');
        }

        const elementRegistry = this.elementRegistry;
        const ids = this.moddle.ids;
        const umlElement = element.umlElement;
        const properties = context.properties;
        const oldProperties = context.oldProperties || getProperties(umlElement, keys(properties));

        if(ID in properties) {

            ids.unclaim(umlElement[ID]);
            elementRegistry.updateId(element, properties[ID]);

        }

        // correctly indicate visual changes on default flow updates
        if(DEFAULT_FLOW in properties) {

            if(properties[DEFAULT_FLOW]) {

                changed.push(elementRegistry.get(properties[DEFAULT_FLOW].id));

            }

            if(umlElement[DEFAULT_FLOW]) {

                changed.push(elementRegistry.get(umlElement[DEFAULT_FLOW].id));

            }
        }

        if(NAME in properties && element.label) {

            changed.push(element.label);
            // show the label
            element.label.hidden = !properties[NAME];

        }

        // update properties
        setProperties(umlElement, properties);

        // store old values
        context.oldProperties = oldProperties;
        context.changed = changed;

        // indicate changed on objects affected by the update
        return changed;

    }

    revert(context) {

        const element = context.element;
        const properties = context.properties;
        const oldProperties = context.oldProperties;
        const umlElement = element.umlElement;
        const elementRegistry = this.elementRegistry;
        const ids = this.moddle.ids;

        // update properties
        setProperties(umlElement, oldProperties);

        if(ID in properties) {

            ids.unclaim(properties[ID]);
            elementRegistry.updateId(element, oldProperties[ID]);

        }

        return context.changed;

    }
}

(UpdatePropertiesHandler as any).$inject = [ 'elementRegistry', 'moddle' ];
