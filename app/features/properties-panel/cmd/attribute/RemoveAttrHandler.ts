//---------------------CLASS---------------------
export default class RemoveAttrHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const oldValue = context.element.attr;
        const index = context.element.elem.umlElement.get('ownedProperty').indexOf(oldValue);

        context.element.elem.umlElement.get('ownedProperty').splice(index, 1);
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.elem.umlElement.get('ownedProperty').push(context.oldProperties);
        return context.changed;

    }
}
