//---------------------CLASS---------------------
export default class UpdateAttrNameHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newName;
        const oldValue = context.element.attr.name;

        context.element.attr.name = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.attr.name = context.oldProperties;
        return context.changed;

    }
}
