//---------------------CLASS---------------------
export default class UpdateAttrTypeHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newType;
        const oldValue = context.element.attr.type;

        context.element.attr.type = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.attr.type = context.oldProperties;
        return context.changed;

    }
}
