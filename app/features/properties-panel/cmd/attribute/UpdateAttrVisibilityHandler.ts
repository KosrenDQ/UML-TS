//---------------------CLASS---------------------
export default class UpdateAttrTypeHandler {


    //---------------------METHODS---------------------
    execute(context) {

        let newValue = '';

        if (context.newVisibility === '+') {

            newValue = 'Public';

        } else if (context.newVisibility === '-') {

            newValue = 'Private';

        } else if (context.newVisibility === '#') {

            newValue = 'Protected';

        }

        const oldValue = context.element.attr.visibility;

        context.element.attr.visibility = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.attr.visibility = context.oldProperties;
        return context.changed;

    }
}
