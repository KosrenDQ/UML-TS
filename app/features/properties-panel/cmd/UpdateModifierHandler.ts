//---------------------CLASS---------------------
export default class UpdateModifierHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newModifier;
        let oldValue = false;

        if (newValue === 'abstract') {

            oldValue = context.element.elem.umlElement.isAbstract;
            context.element.elem.umlElement.isInterface = false;
            context.element.elem.umlElement.isAbstract = true;

        } else if (newValue === 'interface') {

            oldValue = context.element.elem.umlElement.isInterface;
            context.element.elem.umlElement.isInterface = true;
            context.element.elem.umlElement.isAbstract = false;

        } else if (newValue === 'nothing') {

            context.element.elem.umlElement.isAbstract = false;
            context.element.elem.umlElement.isInterface = false;

        }

        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        return context.changed;

    }
}
