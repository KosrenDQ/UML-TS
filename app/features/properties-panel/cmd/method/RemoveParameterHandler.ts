//---------------------CLASS---------------------
export default class RemoveParameterHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const oldValue = context.element.parameter;
        const method = context.element.method;
        const index = method.get('parameter').indexOf(oldValue);

        method.get('parameter').splice(index, 1);
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.method.get('parameter').push(context.oldProperties);
        return context.changed;

    }
}
