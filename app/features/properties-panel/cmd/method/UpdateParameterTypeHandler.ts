//---------------------CLASS---------------------
export default class UpdateParameterTypeHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newType;
        const oldValue = context.element.param.type;

        context.element.param.type = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.param.type = context.oldProperties;
        return context.changed;

    }
}
