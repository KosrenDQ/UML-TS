//---------------------CLASS---------------------
export default class UpdateMethodBodyHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newBody = context.newBody;
        const oldValue = context.element.method.body;

        context.element.method.body = newBody;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.method.body = context.oldProperties;
        return context.changed;

    }
}
