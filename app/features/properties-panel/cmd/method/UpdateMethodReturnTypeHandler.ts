//---------------------CLASS---------------------
export default class UpdateMethodReturnTypeHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newType;
        const oldValue = context.element.method.returnType;

        context.element.method.returnType = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.method.returnType = context.oldProperties;
        return context.changed;

    }
}
