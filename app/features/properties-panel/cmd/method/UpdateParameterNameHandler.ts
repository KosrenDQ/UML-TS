//---------------------CLASS---------------------
export default class UpdateParameterNameHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newName;
        const oldValue = context.element.param.name;

        context.element.param.name = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.param.name = context.oldProperties;
        return context.changed;

    }
}
