//---------------------CLASS---------------------
export default class RemoveMethodHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const oldValue = context.element.method;
        const index = context.element.elem.umlElement.get('ownedOperation').indexOf(oldValue);

        context.element.elem.umlElement.get('ownedOperation').splice(index, 1);
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.elem.umlElement.get('ownedOperation').push(context.oldProperties);
        return context.changed;

    }
}
