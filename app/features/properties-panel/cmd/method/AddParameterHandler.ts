//---------------------CLASS---------------------
export default class AddParameterHandler {


    //---------------------CONSTRUCTOR---------------------
    constructor(private moddle) { }


    //---------------------METHODS---------------------
    execute(context) {

        const method = context.element.method;
        const param = this.moddle.create('uml:Property', {name: 'newParam', type: 'String'});
        this.ensureId(param);

        method.get('parameter').push(param);
        context.changed = context.element.elem;

        return context.element.elem;

    }

    private ensureId(element) {

        const prefix = (element.$type || '').replace(/^[^:]*:/g, '') + '_';

        if (!element.id) {

            element.id = this.moddle.ids.nextPrefixed(prefix, element);

        }
    }
}

(AddParameterHandler as any).$inject = [ 'moddle' ];
