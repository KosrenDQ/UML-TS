//---------------------CLASS---------------------
export default class UpdateMethodVisibilityHandler {


    //---------------------METHODS---------------------
    execute(context) {

        let newValue = '';
        if(context.newVisibility === '+') {

            newValue = 'Public';

        } else if(context.newVisibility === '-') {

            newValue = 'Private';

        } else if(context.newVisibility === '#') {

            newValue = 'Protected';

        }

        const oldValue = context.element.method.visibility;

        context.element.method.visibility = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.method.visibility = context.oldProperties;
        return context.changed;

    }
}
