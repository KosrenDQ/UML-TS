//---------------------CLASS---------------------
export default class UpdateMethodNameHandler {


    //---------------------METHODS---------------------
    execute(context) {

        const newValue = context.newName;
        const oldValue = context.element.method.name;

        context.element.method.name = newValue;
        context.oldProperties = oldValue;
        context.changed = context.element.elem;

        return context.element.elem;

    }

    revert(context) {

        context.element.method.name = context.oldProperties;
        return context.changed;

    }
}

