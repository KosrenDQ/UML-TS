//---------------------IMPORTS---------------------
import { forEach } from 'lodash';
import * as $ from 'jquery';
import { is } from '../../util/ModelUtil';
import UpdatePropertiesHandler from './cmd/UpdatePropertiesHandler';
import UpdateModifierHandler from './cmd/UpdateModifierHandler';
import UpdateAssocSourceCardHandler from './cmd/association/UpdateAssocSourceCardHandler';
import UpdateAssocSourceNameHandler from './cmd/association/UpdateAssocSourceNameHandler';
import UpdateAssocTargetCardHandler from './cmd/association/UpdateAssocTargetCardHandler';
import UpdateAssocTargetNameHandler from './cmd/association/UpdateAssocTargetNameHandler';
import RemoveAttrHandler from './cmd/attribute/RemoveAttrHandler';
import UpdateAttrNameHandler from './cmd/attribute/UpdateAttrNameHandler';
import UpdateAttrTypeHandler from './cmd/attribute/UpdateAttrTypeHandler';
import UpdateAttrVisibilityHandler from './cmd/attribute/UpdateAttrVisibilityHandler';
import AddParameterHandler from './cmd/method/AddParameterHandler';
import RemoveMethodHandler from './cmd/method/RemoveMethodHandler';
import RemoveParameterHandler from './cmd/method/RemoveParameterHandler';
import UpdateMethodBodyHandler from './cmd/method/UpdateMethodBodyHandler';
import UpdateMethodNameHandler from './cmd/method/UpdateMethodNameHandler';
import UpdateMethodReturnTypeHandler from './cmd/method/UpdateMethodReturnTypeHandler';
import UpdateMethodVisibilityHandler from './cmd/method/UpdateMethodVisibilityHandler';
import UpdateParameterNameHandler from './cmd/method/UpdateParameterNameHandler';
import UpdateParameterTypeHandler from './cmd/method/UpdateParameterTypeHandler';
import UpdateLiteralNameHandler from './cmd/literal/UpdateLiteralNameHandler';
import RemoveLiteralHandler from './cmd/literal/RemoveLiteralHandler';
import Label = DiagramJS.Model.Label;


//---------------------CLASS---------------------
export default class PropertiesPanel {


    //---------------------CONSTRUCTOR---------------------
    constructor(config, private eventBus, private modeling, private commandStack, private canvas, private graphicsFactory) {

        this.init(config);

    }

    public registerCmdHandlers() {

        forEach(this.getCmdHandlers(), (handler, id) => {
            this.commandStack.registerHandler(id, handler);
        });
    }

    public getCmdHandlers = function () {
        return {

            'element.updateProperties': UpdatePropertiesHandler,

            'element.property.visibility': UpdateAttrVisibilityHandler,
            'element.property.type': UpdateAttrTypeHandler,
            'element.property.name': UpdateAttrNameHandler,
            'element.property.remove': RemoveAttrHandler,

            'element.method.visibility': UpdateMethodVisibilityHandler,
            'element.method.name': UpdateMethodNameHandler,
            'element.method.body': UpdateMethodBodyHandler,
            'element.method.returnType': UpdateMethodReturnTypeHandler,
            'element.method.remove': RemoveMethodHandler,
            'element.method.parameter.add': AddParameterHandler,
            'element.method.parameter.remove': RemoveParameterHandler,
            'element.method.parameter.type': UpdateParameterTypeHandler,
            'element.method.parameter.name': UpdateParameterNameHandler,

            'element.modifier': UpdateModifierHandler,
            
            'element.literal.name': UpdateLiteralNameHandler,
            'element.literal.remove': RemoveLiteralHandler,

            'element.association.source.name': UpdateAssocSourceNameHandler,
            'element.association.source.cardinality': UpdateAssocSourceCardHandler,
            'element.association.target.name': UpdateAssocTargetNameHandler,
            'element.association.target.cardinality': UpdateAssocTargetCardHandler

        };
    };

    private init(config) {

        const eventBus = this.eventBus;

        eventBus.on('diagram.init', () => {
            this.registerCmdHandlers();
        });

        const panel = $(config.parent);
        const events = [
            'element.click',
            'element.dblclick',
            'element.attribute.new',
            'element.method.new',
            'element.literal.new',
            'element.method.parameter.add'
        ];

        events.forEach((event) => {

            eventBus.on(event, (e) => {

                if (e.element && e.element.type !== 'umldi:UMLDiagram' && e.element.type !== 'label') {

                    this.refreshPropertiesPanel(panel, e.element);

                } else {

                    panel.empty();

                }
            });
        });
    }

    private refreshPropertiesPanel(panel, element) {

        panel.empty();
        if(element.umlElement && element.umlElement.$typ !== 'umldi:UMLDiagram') {

            this.addTitle(panel);
            this.addProperties(element, panel);

        }
    }

    private addTitle(container) {

        const titleHtml = '<h3 class="pp-title">Element Properties:</h3><hr /><br>';
        container.append(titleHtml);

    }
    
    private addElementID(element, container) {
        
        const idHtml = `
            <label for="${element.id}-id"> Element ID:</label>
            <br> 
            <div class="pp-field-wrapper">
                <input id="${element.id}-id" type="text" name="Element ID" value="${element.id}"/>
            </div>`;

        container.append(idHtml);
        this.addIDListener($('#' + element.id + '-id'), element);
        
    }
    
    private addElementName(element, container) {

        if(element.umlElement.name) {

            const shortType = element.umlElement.$type.replace(/^uml\:/, '');

            const nameHtml = `
                <br>
                <label for="${element.id}-name">${shortType} Name:</label>
                <br>
                <div class="pp-field-wrapper">
                    <input id="${element.id}-name" type="text" name="Class Name" value="${element.umlElement.name}"/>
                </div>`;

            container.append(nameHtml);
            this.addNameListener($('#' + element.id + '-name'), element);

        }
    }
    
    private addClassAttributes(element, container) {

        const attributes = element.umlElement.get('ownedProperty');

        let attrHtml = `
                <br>
                <label for="${element.id}-attributes">Attributes:</label><br>
                <div class="pp-field-wrapper">`;

        for (let attr of attributes) {

            attrHtml += `
                    <div id="${attr.id}" style="margin-bottom: 10px;">
                        <select id="${attr.id}-visibility" title="Attribute Visibility">`;

            if(attr.visibility === 'Private') {

                attrHtml += `<option title="Private" value="-" selected>-</option>`;

            } else {

                attrHtml += `<option title="Private" value="-">-</option>`;

            }

            if(attr.visibility === 'Public') {

                attrHtml += `<option title="Public" value="+" selected>+</option>`;

            } else {

                attrHtml += `<option title="Public" value="+">+</option>`;

            }

            if(attr.visibility === 'Protected') {

                attrHtml += `<option title="Protected" value="#" selected>#</option>`;

            } else {

                attrHtml += `<option title="Protected" value="#">#</option>`;

            }

            attrHtml += `
                    </select>
                    <input id="${attr.id}-name" title="Attribute Name" style="max-width: 85px;" type="text" name="Attribute Name" value="${attr.name}"/>
                    :
                    <input id="${attr.id}-type" title="Attribute Type" style="max-width: 50px;" type="text" name="Attribute Type" value="${attr.type}"/>
                    <button id="${attr.id}-remove" title="Remove">-</button>
                    </div>`;

        }

        attrHtml += `</div></div>`;
        container.append(attrHtml);

        for(let a of attributes) {

            this.addAttributeVisibilityListener($('#' + a.id + '-visibility'), a, element);
            this.addAttributeTypeListener($('#' + a.id + '-type'), a, element);
            this.addAttributeNameListener($('#' + a.id + '-name'), a, element);
            this.addAttributeRemoveListener($('#' + a.id + '-remove'), a, element);

        }
    }
    
    private addClassMethods(element, container) {

        const methods = element.umlElement.get('ownedOperation');

        let methodHtml = `
                <br>
                <label for="${element.id}-methods">Methods:</label>
                <br>
                <div class="pp-field-wrapper">`;

        for (let met of methods) {

            methodHtml += `
                    <div id="${met.id}" style="margin-bottom: 8px;">
                        <select id="${met.id}-visibility" title="Method Visibility">`;

            if(met.visibility === 'Private') {

                methodHtml += `<option title="Private" value="-" selected>-</option>`;

            } else {

                methodHtml += `<option title="Private" value="-">-</option>`;

            }

            if(met.visibility === 'Public') {

                methodHtml += `<option title="Public" value="+" selected>+</option>`;

            } else {

                methodHtml += `<option title="Public" value="+">+</option>`;

            }

            if(met.visibility === 'Protected') {

                methodHtml += `<option title="Protected" value="#" selected>#</option>`;

            } else {

                methodHtml += `<option title="Protected" value="#">#</option>`;

            }

            methodHtml += `
                    </select>
                    <input id="${met.id}-name" title="Method Name" type="text" name="Method Name" style="max-width: 85px;" value="${met.name}"/>
                    :
                    <input id="${met.id}-returnType" title="Method Return Type" style="max-width: 50px;" type="text" name="Method Return Type" value="${met.returnType}"/>
                    <button id="${met.id}-remove" title="Remove">-</button>
                    <br>`;

            for(let param of met.get('parameter')) {

                methodHtml += `
                        <div id="${param.id}">
                            <input id="${param.id}-name" title="Parameter Name" type="text" name="Parameter Name" style="max-width: 85px; margin-left: 40px;" value="${param.name}"/>
                            :
                            <input id="${param.id}-type" title="Parameter Type" type="text" name="Parameter Type" style="max-width: 50px;" value="${param.type}"/>
                            <button id="${param.id}-remove" title="Remove">-</button>
                            <br>
                        </div>`;

            }

            methodHtml += `
                    <button id="${met.id}-addParam" title="Add Parameter" style="margin-left: 40px;">+</button>
                    </div>`;

        }

        methodHtml += `</div></div>`;
        container.append(methodHtml);

        for(let me of methods) {

            this.addMethodVisibilityListener($('#' + me.id + '-visibility'), me, element);
            this.addMethodNameListener($('#' + me.id + '-name'), me, element);
            this.addMethodBodyListener($('#' + me.id + '-body'), me, element);
            this.addMethodReturnTypeListener($('#' + me.id + '-returnType'), me, element);
            this.addMethodRemoveListener($('#' + me.id + '-remove'), me, element);
            this.addMethodAddParamListener($('#' + me.id + '-addParam'), me, element);

            for(let pa of me.get('parameter')) {

                this.addMethodRemoveParamListener($('#' + pa.id + '-remove'), pa, me, element);
                this.addParameterTypeListener($('#' + pa.id + '-type'), pa, element);
                this.addParameterNameListener($('#' + pa.id + '-name'), pa, element);

            }
        }
    }

    private addClassOptions(element, container) {

        let modifierHtml = `
                <br>
                <label for="${element.id}-modifier">Modifier:</label>
                <br>
                <div class="pp-field-wrapper">
                    <fieldset id="${element.id}-modifier">`;

        if(!element.umlElement.isAbstract && !element.umlElement.isInterface) {

            modifierHtml += `
                    <input type=radio id="${element.id}-nothing" name="modifier" value="nothing" checked/>
                    <label for="${element.id}-nothing">Nothing</label>`;

        } else {

            modifierHtml += `
                    <input type=radio id="${element.id}-nothing" name="modifier" value="nothing"/>
                    <label for="${element.id}-nothing">Nothing</label>`;

        }

        modifierHtml += `<br>`;

        if(element.umlElement.isAbstract) {

            modifierHtml += `
                    <input type=radio id="${element.id}-isAbstract" name="modifier" value="abstract" checked/>
                    <label for="${element.id}-isAbstract">Abstract</label>`;

        } else {

            modifierHtml += `
                    <input type=radio id="${element.id}-isAbstract" name="modifier" value="abstract"/>
                    <label for="${element.id}-isAbstract">Abstract</label>`;

        }

        modifierHtml += `<br>`;

        if(element.umlElement.isInterface) {

            modifierHtml += `
                    <input type=radio id="${element.id}-isInterface" name="modifier" value="interface" checked/>
                    <label for="${element.id}-isInterface">Interface</label>`;

        } else {

            modifierHtml += `
                    <input type=radio id="${element.id}-isInterface" name="modifier" value="interface"/>
                    <label for="${element.id}-isInterface">Interface</label>`;

        }

        modifierHtml += `
                </fieldset>
                </div>`;
        container.append(modifierHtml);

        this.addModifierListener($('#' + element.id + '-nothing'), element);
        this.addModifierListener($('#' + element.id + '-isAbstract'), element);
        this.addModifierListener($('#' + element.id + '-isInterface'), element);
        
    }
    
    private addEnumLiterals(element, container) {
        
        const literals = element.umlElement.get('ownedLiteral');

        let litHtml = `
                <br>
                <label for="${element.id}-literals">Literals:</label><br>
                <div class="pp-field-wrapper">`;
        
        for(let lit of literals) {

            litHtml += `
                    <div>
                    <input id="${lit.id}-name" title="Literal Name" type="text" name="Literal Name" value="${lit.name}"/>
                    <button id="${lit.id}-remove" title="Remove">-</button>
                    </div>`;
            
        }
        
        litHtml += `</div>`;
        container.append(litHtml);
        
        for(let l of literals) {
     
            this.addLiteralNameListener($('#' + l.id + '-name'), l, element);
            this.addLiteralRemoveListener($('#' + l.id + '-remove'), l, element);
            
        }
    }
    
    private addAssociation(element, container) {

        const assoc = element.umlElement;

        let assocHtml = `
                <br>
                <label for="${assoc.id}-source">Source:</label>
                <br>
                <div class="pp-field-wrapper">
                Class: ${assoc.sourceClass.name}
                <br>
                <input id="${assoc.id}-sourceName" title="Source Name" type="text" name="Source Name" value="${assoc.sourceName}"/>
                <br>
                <select id="${assoc.id}-sourceCard" title="Source Cardinality">`;

        if(assoc.sourceCard === 'One') {

            assocHtml += `<option value="One" selected>One</option>`;

        } else {

            assocHtml += `<option value="One">One</option>`;

        }

        if(assoc.sourceCard === 'Many') {

            assocHtml += `<option value="Many" selected>Many</option>`;

        } else {

            assocHtml += `<option value="Many">Many</option>`;

        }

        assocHtml += `
                </select>
                </div>
                <br>
                <label for="${assoc.id}-target">Target:</label>
                <br>
                <div class="pp-field-wrapper">
                Class: ${assoc.targetClass.name}
                <br>
                <input id="${assoc.id}-targetName" title="Target Name" type="text" name="Target Name" value="${assoc.targetName}"/>
                <br>
                <select id="${assoc.id}-targetCard" title="Target Cardinality">`;

        if(assoc.targetCard === 'One') {

            assocHtml += `<option value="One" selected>One</option>`;

        } else {

            assocHtml += `<option value="One">One</option>`;

        }

        if(assoc.targetCard === 'Many') {

            assocHtml += `<option value="Many" selected>Many</option>`;

        } else {

            assocHtml += `<option value="Many">Many</option>`;

        }

        assocHtml += `
                </select>
                </div>`;

        container.append(assocHtml);

        this.addAssocSourceNameListener($('#' + assoc.id + '-sourceName'), assoc, element);
        this.addAssocSourceCardinalityListener($('#' + assoc.id + '-sourceCard'), assoc, element);
        this.addAssocTargetNameListener($('#' + assoc.id + '-targetName'), assoc, element);
        this.addAssocTargetCardinalityListener($('#' + assoc.id + '-targetCard'), assoc, element);
        
    }
    
    private addSuperClass(element, container) {

        const sup = element.umlElement;

        let supHtml = `
                <br>
                <label for="${sup.id}-childClass">Child:</label>
                <br>
                <div class="pp-field-wrapper">
                Class: ${sup.childClass.name}
                </div>
                <br>
                <label for="${sup.id}-fatherClass">Father:</label>
                <br>
                <div class="pp-field-wrapper">
                Class: ${sup.fatherClass.name}
                </div>`;
        
        container.append(supHtml);
        
    }
    
    private addRelation(element, container) {

        const rel = element.umlElement;

        let relHtml = `
                <br>
                <label for="${rel.id}-parentClass">Parent:</label>
                <br>
                <div class="pp-field-wrapper">
                Class: ${rel.parentClass.name}
                </div>
                <br>
                <label for="${rel.id}-childEnum">Child:</label>
                <br>
                <div class="pp-field-wrapper">
                Enum: ${rel.childEnum.name}
                </div>`;

        container.append(relHtml);
        
    }
    
    private addProperties(element, container) {

        this.addElementID(element, container);
        this.addElementName(element, container);

        if (is(element.umlElement, 'uml:Class')) {

            this.addClassAttributes(element, container);
            this.addClassMethods(element, container);
            this.addClassOptions(element, container);

        } else if (is(element.umlElement, 'uml:Enum')) {

            this.addEnumLiterals(element, container);

        } else if (is(element.umlElement, 'uml:Association')) {

            this.addAssociation(element, container);

        } else if (is(element.umlElement, 'uml:SuperClass')) {

            this.addSuperClass(element, container);

        } else if (is(element.umlElement, 'uml:Relation')) {

            this.addRelation(element, container);

        }
    }

    //===================== Element ID listener =====================
    private addIDListener(node, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.updateProperties', {
                    element: element,
                    properties: {id: node.val()}
                });
            }
        });
    }

    //===================== Element name listener =====================
    private addNameListener(node, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.updateLabel', {
                    element: element,
                    newLabel: node.val()
                });
            }
        });
    }

    //===================== Attribute listeners =====================
    private addAttributeVisibilityListener(node, attribute, element) {

        node.bind({
            'change': () => {
                this.commandStack.execute('element.property.visibility', {
                    element: {attr: attribute, elem: element},
                    newVisibility: node.val()
                });
            }
        });
    }

    private addAttributeTypeListener(node, attribute, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.property.type', {
                    element: {attr: attribute, elem: element},
                    newType: node.val()
                });
            }
        });
    }

    private addAttributeNameListener(node, attribute, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.property.name', {
                    element: {attr: attribute, elem: element},
                    newName: node.val()
                });
            }
        });
    }

    private addAttributeRemoveListener(node, attribute, element) {

        node.bind({
            'click': (event) => {

                const allAttr = event.currentTarget.parentNode.parentNode;
                const prop = event.currentTarget.parentNode;

                allAttr.removeChild(prop);

                this.commandStack.execute('element.property.remove', {
                    element: {attr: attribute, elem: element}
                });
            }
        });
    }

    //===================== Literal listeners =====================
    private addLiteralNameListener(node, literal, element) {
        
        node.bind({
            'input': () => {
                this.commandStack.execute('element.literal.name', {
                    element: {literal: literal, elem: element},
                    newName: node.val().toUpperCase()
                });
            }       
        });
    }
    
    private addLiteralRemoveListener(node, literal, element) {
        
        node.bind({
            'click': (event) => {
            
                const allLits = event.currentTarget.parentNode.parentNode;
                const prop = event.currentTarget.parentNode;
               
                allLits.removeChild(prop);
               
                this.commandStack.execute('element.literal.remove', {
                    element: {literal: literal, elem: element}
                });
            } 
        });
    }

    //===================== Methods listeners =====================
    private addMethodVisibilityListener(node, method, element) {

        node.bind({
            'change': () => {
                this.commandStack.execute('element.method.visibility', {
                    element: {method: method, elem: element},
                    newVisibility: node.val()
                });
            }
        });
    }

    private addMethodReturnTypeListener(node, method, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.method.returnType', {
                    element: {method: method, elem: element},
                    newType: node.val()
                });
            }
        });
    }

    private addMethodNameListener(node, method, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.method.name', {
                    element: {method: method, elem: element},
                    newName: node.val()
                });
            }
        });
    }

    private addMethodBodyListener(node, method, element) {

        node.bind({
            'change': () => {
                this.commandStack.execute('element.method.body', {
                    element: {method: method, elem: element},
                    newBody: node.val()
                });
            }
        });
    }

    private addMethodAddParamListener(node, method, element) {

        node.bind({
            'click': () => {
                this.commandStack.execute('element.method.parameter.add', {
                    element: {method: method, elem: element}
                });
                this.eventBus.fire('element.method.parameter.add', {element: element});
            }
        });
    }

    private addMethodRemoveParamListener(node, parameter, method, element) {

        node.bind({
            'click': (event) => {

                const allParams = event.currentTarget.parentNode.parentNode;
                const prop = event.currentTarget.parentNode;

                allParams.removeChild(prop);

                this.commandStack.execute('element.method.parameter.remove', {
                    element: {parameter: parameter, method: method, elem: element}
                });
            }
        });
    }

    private addMethodRemoveListener(node, method, element) {

        node.bind({
            'click': (event) => {

                const allMethods = event.currentTarget.parentNode.parentNode;
                const prop = event.currentTarget.parentNode;

                allMethods.removeChild(prop);

                this.commandStack.execute('element.method.remove', {
                    element: {method: method, elem: element}
                });
            }
        });
    }

    private addParameterTypeListener(node, parameter, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.method.parameter.type', {
                    element: {param: parameter, elem: element},
                    newType: node.val()
                });
            }
        });
    }

    private addParameterNameListener(node, parameter, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.method.parameter.name', {
                    element: {param: parameter, elem: element},
                    newName: node.val()
                });
            }
        });
    }

    //===================== Modifier listeners =====================
    private addModifierListener(node, element) {

        node.bind({
            'change': () => {
                this.commandStack.execute('element.modifier', {
                    element: {elem: element},
                    newModifier: node.val()
                });

                element.incoming.forEach((connection) => {

                    if(connection.type === 'uml:SuperClass') {

                        this.graphicsFactory.update('connection', connection, this.canvas.getGraphics(connection));

                    }
                });
            }
        });
    }

    //===================== Association listeners =====================
    private addAssocSourceNameListener(node, assoc, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.association.source.name', {
                    element: {assoc: assoc, elem: element},
                    newName: node.val()
                });

                this.commandStack.execute('element.updateLabel', {
                    element: {element: element, label: this.getLabel(element, 'sourceName')},
                    newLabel: node.val()
                });
            }
        });
    }

    private addAssocSourceCardinalityListener(node, assoc, element) {

        node.bind({
            'change': () => {
                this.commandStack.execute('element.association.source.cardinality', {
                    element: {assoc: assoc, elem: element},
                    newCard: node.val()
                });

                this.commandStack.execute('element.updateLabel', {
                    element: {element: element, label: this.getLabel(element, 'sourceCard')},
                    newLabel: node.val()
                });
            }
        });
    }

    private addAssocTargetNameListener(node, assoc, element) {

        node.bind({
            'input': () => {
                this.commandStack.execute('element.association.target.name', {
                    element: {assoc: assoc, elem: element},
                    newName: node.val()
                });

                this.commandStack.execute('element.updateLabel', {
                    element: {element: element, label: this.getLabel(element, 'targetName')},
                    newLabel: node.val()
                });
            }
        });
    }

    private addAssocTargetCardinalityListener(node, assoc, element) {

        node.bind({
            'change': () => {
                this.commandStack.execute('element.association.target.cardinality', {
                    element: {assoc: assoc, elem: element},
                    newCard: node.val()
                });

                this.commandStack.execute('element.updateLabel', {
                    element: {element: element, label: this.getLabel(element, 'targetCard')},
                    newLabel: node.val()
                });
            }
        });
    }
    
    private getLabel(element, assocDock) {
        
        for(let l of element.labels) {
            
            if(l.assocDock === assocDock) {
                
                return l;
                
            }
        }
        
        return null;
        
    }
}

(PropertiesPanel as any).$inject = [ 'config.propertiesPanel', 'eventBus', 'modeling', 'commandStack', 'canvas', 'graphicsFactory' ];
