//---------------------IMPORT---------------------
import BaseModeling = require('diagram-js/lib/features/modeling/Modeling');
import AddClassAttributeHandler = require('./cmd/AddClassAttributeHandler');
import AddClassMethodHandler = require('./cmd/AddClassMethodHandler');
import AddEnumLiteralHandler = require('./cmd/AddEnumLiteralHandler');


//---------------------CLASS---------------------
export default class Modeling extends BaseModeling {


    //---------------------CONSTRUCTOR---------------------
    constructor(private eventBus, elementFactory, private commandStack, private umlRules) {
        super(eventBus, elementFactory, commandStack);

        commandStack.registerHandler('element.attribute.new', AddClassAttributeHandler.default);
        commandStack.registerHandler('element.method.new', AddClassMethodHandler.default);
        commandStack.registerHandler('element.literal.new', AddEnumLiteralHandler.default);

    }


    //---------------------METHODS---------------------
    addClassAttribute(element) {

        this.commandStack.execute('element.attribute.new', {element: element});
        this.eventBus.fire('element.attribute.new', {element: element});
        
    }

    addClassMethod(element) {

        this.commandStack.execute('element.method.new', {element: element});
        this.eventBus.fire('element.method.new', {element: element});
        
    }
    
    addEnumLiteral(element) {
        
        this.commandStack.execute('element.literal.new', {element: element});
        this.eventBus.fire('element.literal.new', {element: element});
        
    }

    connect(source, target, attrs) {

        if (!attrs) {

            attrs = this.umlRules.canConnect(source, target);

        }
        if (attrs) {

            return this.createConnection(source, target, attrs, source.parent);

        }
    }
}

(Modeling as any).$inject = [ 'eventBus', 'elementFactory', 'commandStack', 'umlRules' ];
