//---------------------IMPORTS---------------------
import ElementFactory from './ElementFactory';
import UmlLayouter from './UmlLayouter';
import UmlFactory from './UmlFactory';
import UmlLabelSupport from './UmlLabelSupport';
import UmlUpdater from './UmlUpdater';
import Modeling from './Modeling';


//---------------------EXPORTS---------------------
module.exports = {

    __init__: [ 'modeling', 'umlUpdater', 'umlLabelSupport' ],

    __depends__: [
        require('../label-editing'),
        require('../rules'),
        require('diagram-js/lib/command'),
        require('diagram-js/lib/features/tooltips'),
        require('diagram-js/lib/features/label-support'),
        require('diagram-js/lib/features/attach-support'),
        require('diagram-js/lib/features/selection'),
        require('diagram-js/lib/features/change-support'),
        require('diagram-js/lib/features/space-tool')
    ],

    elementFactory: [ 'type', ElementFactory ],
    layouter: [ 'type', UmlLayouter ],
    umlFactory: [ 'type', UmlFactory ],
    umlLabelSupport: [ 'type', UmlLabelSupport ],
    umlUpdater: [ 'type', UmlUpdater ],
    modeling: [ 'type', Modeling ],
    connectionDocking: [ 'type', require('diagram-js/lib/layout/CroppingConnectionDocking') ]
};
