//---------------------IMPORTS---------------------
import { assign } from 'lodash';
import * as LabelUtil from '../label-editing/LabelUtil';
import BaseElementFactory = require('diagram-js/lib/core/ElementFactory');


//---------------------CLASS---------------------
export default class ElementFactory extends BaseElementFactory {


    //---------------------CONSTRUCTOR---------------------
    constructor(private umlFactory, private moddle) {

        super();

    }


    //---------------------METHODS---------------------
    create(elementType: string, attrs: any) {

        let size;
        let umlElement = attrs.umlElement;
        attrs = attrs || {};

        if(elementType === 'label') {

            return super.create(elementType, assign({type: 'label'}, LabelUtil.DEFAULT_LABEL_SIZE, attrs));

        }

        const self = this;

        if(elementType === 'root') {

            return super.create(elementType, attrs);

        }

        if(!umlElement) {

            if(!attrs.type) {

                throw new Error('no shape type specified');

            }

            umlElement = self.umlFactory.create(attrs.type);
            if(umlElement.$instanceOf('uml:Class')) {

                const shortType = umlElement.$type.replace(/^uml\:/, '');
                umlElement.name = 'new' + shortType;
                umlElement.isAbstract = false;
                umlElement.isInterface = false;

            } else if(umlElement.$instanceOf('uml:Enum')) {

                const shortType = umlElement.$type.replace(/^uml\:/, '');
                umlElement.name = 'new' + shortType;

            } else if(umlElement.$instanceOf('uml:Association')) {

                umlElement.sourceName = 'sourceName';
                umlElement.sourceCard = 'One';
                umlElement.targetName = 'targetName';
                umlElement.targetCard = 'One';

            }
        }

        if(!umlElement.di) {

            if(elementType === 'connection') {

                umlElement.di = self.umlFactory.createDiEdge(umlElement, [], {

                    id: umlElement.id + '_di'

                });

            } else if(elementType === 'shape') {

                if(umlElement.$instanceOf('uml:Class') || umlElement.$instanceOf('uml:Enum')) {

                    umlElement.di = self.umlFactory.createDiShape(umlElement, {}, {

                        id: umlElement.id + '_di'

                    });
                }
            }
        }

        size = this._getDefaultSize(umlElement);

        attrs = assign({
            umlElement: umlElement,
            id: umlElement.id
        }, size, attrs);

        return super.create(elementType, attrs);

    }

    protected _getDefaultSize(semantic) {

        if (semantic.$instanceOf('uml:Class') || semantic.$instanceOf('uml:Enum')) {

            return {width: 150, height: 100};

        }

        return {width: 100, height: 80};

    }
}

(ElementFactory as any).$inject = [ 'umlFactory', 'moddle' ];
