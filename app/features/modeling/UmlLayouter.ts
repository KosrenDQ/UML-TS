//---------------------IMPORTS---------------------
import BaseLayouter = require('diagram-js/lib/layout/BaseLayouter');
import * as ManhattanLayout from 'diagram-js/lib/layout/ManhattanLayout';
import { getMid } from 'diagram-js/lib/layout/LayoutUtil';
import { assign } from 'lodash';


//---------------------CLASS---------------------
export default class UmlLayouter extends BaseLayouter {


    //---------------------METHODS---------------------
    layoutConnection(connection, hints) {

        hints = hints || {};

        const source = connection.source;
        const target = connection.target;
        const waypoints = connection.waypoints;

        let start = hints.connectionStart;
        let end = hints.connectionEnd;
        let manhattanOptions = {
            preferredLayouts: [ 'v:v' ]
        };

        if(!start) {

            start = this.getConnectionDocking(waypoints && waypoints[0], source);

        }

        if(!end) {

            end = this.getConnectionDocking(waypoints && waypoints[waypoints.length - 1], target);

        }

        manhattanOptions = assign(manhattanOptions, hints);

        if (source === target) {

            start.x += 30;
            end.x -= 30;
            const updatedWaypoints = ManhattanLayout.repairConnection(source, target, start, end, waypoints, manhattanOptions);

            if (!updatedWaypoints) {

                const p1 = {x: 0, y: 0};
                const p2 = {x: 0, y: 0};

                p1.x = start.x;
                p1.y = start.y - 100;
                p2.x = end.x;
                p2.y = end.y - 100;

                return [start, p1, p2, end];

            }

            return [ start, end ];

        } else {

            return ManhattanLayout.repairConnection(source, target, start, end, waypoints, manhattanOptions) || [ start, end ];

        }
    }

    private getConnectionDocking(point, shape) {

        return point ? (point.original || point) : getMid(shape);

    }
}
