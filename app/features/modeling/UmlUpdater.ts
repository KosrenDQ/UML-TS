//---------------------IMPORT---------------------
import { assign } from 'lodash';
import { is, getUmlElement } from '../../util/ModelUtil';
import * as Collections from 'diagram-js/lib/util/Collections';
import CommandInterceptor = require('diagram-js/lib/command/CommandInterceptor');


//---------------------CLASS---------------------
export default class UmlUpdater extends CommandInterceptor {


    //---------------------CONSTRUCTOR---------------------
    constructor(eventBus, private umlFactory, connectionDocking) {
        super(eventBus);
        const self = this;

        // crop connection ends during create/update
        this.on([ 'connection.layout', 'connection.create', 'connection.reconnectEnd', 'connection.reconnectStart' ], 'executed', cropConnection);
        this.on([ 'connection.layout' ], 'reverted', (e) => { delete e.context.cropped; });

        function cropConnection(e) {

            const context = e.context;

            if(!context.cropped) {

                const connection = context.connection;
                connection.waypoints = connectionDocking.getCroppedWaypoints(connection);
                context.cropped = true;

            }
        }

        // update updateConnection
        this.on([ 'connection.create', 'connection.move', 'connection.delete', 'connection.reconnectEnd', 'connection.reconnectStart' ], 'executed', updateConnection);
        this.on([ 'connection.create', 'connection.move', 'connection.delete', 'connection.reconnectEnd', 'connection.reconnectStart' ], 'reverted', updateConnection);

        function updateConnection(e) {

            const context = e.context;
            self.updateConnection(context);

        }

        // update waypoints
        this.on([ 'connection.layout', 'connection.move', 'connection.updateWaypoints', 'connection.reconnectEnd', 'connection.reconnectStart' ], 'executed', updateConnectionWaypoints);
        this.on([ 'connection.layout', 'connection.move', 'connection.updateWaypoints', 'connection.reconnectEnd', 'connection.reconnectStart' ], 'reverted', updateConnectionWaypoints);

        function updateConnectionWaypoints(e) {

            self.updateConnectionWaypoints(e.context.connection);

        }

        // update parent
        function updateParent(e) {

            const context = e.context;
            self.updateParent(context.shape || context.connection, context.oldParent);

        }

        function reverseUpdateParent(e) {

            const context = e.context;
            const element = context.shape || context.connection;
            const oldParent = context.parent || context.newParent;
            self.updateParent(element, oldParent);

        }

        this.on([ 'shape.move', 'shape.create', 'shape.delete', 'connection.create', 'connection.move', 'connection.delete' ], 'executed', updateParent);
        this.on([ 'shape.move', 'shape.create', 'shape.delete', 'connection.create', 'connection.move', 'connection.delete' ], 'reverted', reverseUpdateParent);

        // update bounds
        function updateBounds(e) {

            const shape = e.context.shape;
            self.updateBounds(shape);

        }

        this.on([ 'shape.move', 'shape.create', 'shape.resize' ], 'executed', updateBounds);
        this.on([ 'shape.move', 'shape.create', 'shape.resize' ], 'reverted', updateBounds);

    }

    updateConnection(context) {

        const connection = context.connection;
        const umlElement = getUmlElement(connection);
        const newSource = getUmlElement(connection.source);
        const newTarget = getUmlElement(connection.target);

        if(is(umlElement, 'uml:Association')) {

            if(newSource && newTarget) {

                if(!umlElement.$parent || !umlElement.di.$parent) {

                    const rootShape = this.getRootFromElement(connection);
                    const parentUE = this.getUMLModel(rootShape);
                    umlElement.$parent = this.getUMLModel(rootShape);
                    parentUE.get('ownedAssociation').push(umlElement);

                }

                const modelRoot = this.getModelRootFromUe(umlElement);
                const parentDI = this.getCurrentRootDIContainer(modelRoot);
                this.updateParentDI(umlElement.di, parentDI);
                umlElement.sourceClass = newSource;
                umlElement.targetClass = newTarget;

            } else {

                this.updateParentDI(umlElement.di);
                this.updateParentUmlElement(umlElement);

            }

        } else if(is(umlElement, 'uml:SuperClass')) {

            if(newSource && newTarget) {

                if(!umlElement.$parent || !umlElement.di.$parent) {

                    const rootShapeS = this.getRootFromElement(connection);
                    const parentUES = this.getUMLModel(rootShapeS);
                    umlElement.$parent = this.getUMLModel(rootShapeS);
                    parentUES.get('ownedSuperClass').push(umlElement);

                }

                const modelRootS = this.getModelRootFromUe(umlElement);
                const parentDIS = this.getCurrentRootDIContainer(modelRootS);
                this.updateParentDI(umlElement.di, parentDIS);
                umlElement.childClass = newSource;
                umlElement.fatherClass = newTarget;

            } else {

                this.updateParentDI(umlElement.di);
                this.updateParentUmlElement(umlElement);

            }
            
        } else if(is(umlElement, 'uml:Relation')) {
            
            if(newSource && newTarget) {
         
                if(!umlElement.$parent || !umlElement.di.$parent) {
                
                    const rootShapeR = this.getRootFromElement(connection);
                    const parentUER = this.getUMLModel(rootShapeR);
                    umlElement.$parent = this.getUMLModel(rootShapeR);
                    parentUER.get('ownedRelation').push(umlElement);
                    
                }
                
                const modelRootR = this.getModelRootFromUe(umlElement);
                const parentDIR = this.getCurrentRootDIContainer(modelRootR);
                this.updateParentDI(umlElement.di, parentDIR);
                umlElement.parentClass = newSource;
                umlElement.childEnum = newTarget;
                
            }
        }

        this.updateConnectionWaypoints(connection);

    };

    updateConnectionWaypoints(connection) {

        connection.umlElement.di.set('waypoint', this.umlFactory.createDiWaypoints(connection.waypoints));

    }

    getRootFromElement(el) {

        if(el.parent) {

            return this.getRootFromElement(el.parent);

        }

        return el;

    }

    getModelRootFromUe(ue) {

        if(ue.$parent) {

            return this.getModelRootFromUe(ue.$parent);

        }

        return ue;

    }

    getUMLModel(rootShape) {

        //returns the root bo from first child with a umlElement
        for(let i = 0; i < rootShape.children.length; i++) {

            if(rootShape.children[i].umlElement) {

                return this.getModelRootFromUe(rootShape.children[i].umlElement);

            }
        }
    }

    getCurrentRootDIContainer(modelRoot) {

        return modelRoot.diagram;

    }

    updateBounds(shape) {

        const di = shape.umlElement.di;
        const bounds = di.bounds;

        assign(bounds, {
            x: shape.x,
            y: shape.y,
            width: shape.width,
            height: shape.height
        });
    }

    updateParent(element, oldParent) {

        const parentShape = element.parent;
        let umlElement = getUmlElement(element);
        let parentUmlElement = getUmlElement(element.parent);
        let parentDi = parentUmlElement && parentUmlElement.di;

        if(is(parentUmlElement, 'umldi:UMLDiagram')) {

            parentDi = parentUmlElement;

        }

        if(!parentUmlElement && parentShape) {

            parentUmlElement = this.getUMLModel(parentShape);
            parentDi = this.getCurrentRootDIContainer(parentUmlElement);

        }

        if(is(umlElement, 'uml:Class') || is(umlElement, 'uml:Enum')) {

            this.updateParentUmlElement(umlElement, parentUmlElement);

        }

        this.updateParentDI(umlElement.di, parentDi);

    }

    updateParentUmlElement(umlElement, newParentUmlElement?) {

        const oldParentUmlElement = umlElement.$parent;
        let containment;

        if(oldParentUmlElement === newParentUmlElement) {

            return;

        }

        //add to new parent
        if(newParentUmlElement) {

            if(is(newParentUmlElement, 'uml:ClassGraph')) {

                if(is(umlElement, 'uml:Class')) {

                    containment = 'ownedClass';

                } else if(is(umlElement, 'uml:Enum')) {
                    
                    containment = 'ownedEnumeration';
                
                } else if(is(umlElement, 'uml:Association')) {

                    containment = 'ownedAssociation';

                } else if(is(umlElement, 'uml:SuperClass')) {

                    containment = 'ownedSuperClass';

                } else if(is(umlElement, 'uml:Relation')) {
                    
                    containment = 'ownedRelation';
                    
                }

            } else if(is(newParentUmlElement, 'umldi:UMLDiagram')) {

                newParentUmlElement = this.getModelRootFromUe(newParentUmlElement);
                containment = is(umlElement, 'uml:Class') ? 'ownedClass' : 'ownedEnumeration';

            }

            newParentUmlElement.get(containment).push(umlElement);
            umlElement.$parent = newParentUmlElement;

        } else {

            umlElement.$parent = null;

        }

        //remove from old parent
        if(oldParentUmlElement) {

            if(is(umlElement, 'uml:Class')) {

                containment = 'ownedClass';

            } else if(is(umlElement, 'uml:Enum')) {

                containment = 'ownedEnumeration';

            } else if(is(umlElement, 'uml:Association')) {

                containment = 'ownedAssociation';

            } else if(is(umlElement, 'uml:SuperClass')) {

                containment = 'ownedSuperClass';

            } else if(is(umlElement, 'uml:Relation')) {

                containment = 'ownedRelation';

            }

            Collections.remove(oldParentUmlElement.get(containment), umlElement);

        }
    }

    updateParentDI(di, newParentDi?) {

        const oldParentDi = di.$parent;

        if(di.$parent === newParentDi) {
            return;
        }

        if(newParentDi) {

            if(newParentDi.$type === 'umldi:UMLDiagram') {

                newParentDi.get('diagramElements').push(di);

            } else {

                newParentDi.get('planeElements').push(di);

            }

            di.$parent = newParentDi;

        } else {

            di.$parent = null;

        }

        //remove from old parent
        if(oldParentDi) {

            if(oldParentDi.$type === 'umldi:UMLDiagram') {

                Collections.remove(oldParentDi.get('diagramElements'), di);

            } else {

                Collections.remove(oldParentDi.get('planeElements'), di);

            }
        }
    }
}

(UmlUpdater as any).$inject = [ 'eventBus', 'umlFactory', 'connectionDocking' ];
