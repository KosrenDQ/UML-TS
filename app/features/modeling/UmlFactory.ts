//---------------------IMPORTS---------------------
import { assign, map, pick } from 'lodash';


//---------------------CLASS---------------------
export default class UmlFactory {


    //---------------------CONSTRUCTOR---------------------
    constructor(private moddle) { }


    //---------------------METHOD---------------------
    create(type, attrs) {

        let element = this.moddle.create(type, attrs || {});
        this.ensureId(element);

        return element;

    }

    createDiShape(semantic, bounds, attrs) {

        return this.create('umldi:UMLShape', assign({
            umlElement: semantic,
            bounds: this.createDiBounds(bounds)
        }, attrs));
    }

    createDiContainerShape(semantic, bounds, attrs) {

        return this.create('umldi:UMLPlaneShape', assign({
            umlElement: semantic,
            bounds: this.createDiBounds(bounds)
        }, attrs));
    }

    createDiBounds(bounds?) {

        return this.create('dc:Bounds', bounds);

    }

    createDiWaypoints(waypoints) {

        let self = this;
        return map(waypoints, function (pos) {
            return self.createDiWaypoint(pos);
        });
    }

    createDiWaypoint(point) {

        return this.create('dc:Point', pick(point, [ 'x', 'y' ]));

    }

    createDiEdge(semantic, waypoints, attrs) {

        return this.create('umldi:UMLEdge', assign({
            umlElement: semantic
        }, attrs));
    }

    createDiLabel(semantic) {

        return this.create('umldi:UMLLabel', {
            bounds: this.createDiBounds()
        });
    }

    createDiPlane(semantic) {

        return this.create('umldi:UMLPlane', {
            umlElement: semantic
        });
    }

    private ensureId(element) {

        const prefix = (element.$type || '').replace(/^[^:]*:/g, '') + '_';

        if (!element.id) {
            element.id = this.moddle.ids.nextPrefixed(prefix, element);

        }
    }
}

(UmlFactory as any).$inject = [ 'moddle' ];
