//---------------------CLASS---------------------
export default class AddClassAttributeHandler {


    //---------------------CONSTRUCTOR---------------------
    constructor(private moddle) { }


    //---------------------METHODS---------------------
    execute(context) {

        const umlElement = context.element.umlElement;
        const newAttribute = this.moddle.create('uml:Property', {
            visibility: 'Private',
            type: 'String',
            name: 'newAttribute',
            owner: umlElement
        });
        this.ensureId(newAttribute);

        umlElement.get('ownedProperty').push(newAttribute);

        const length = umlElement.get('ownedProperty').length + umlElement.get('ownedOperation').length;

        if (length > 4) {

            context.element.height = 100 + (length - 4) * 14;

        }

        return context.element;

    }

    private ensureId(element) {

        const prefix = (element.$type || '').replace(/^[^:]*:/g, '') + '_';

        if (!element.id) {

            element.id = this.moddle.ids.nextPrefixed(prefix, element);

        }
    }
}

(AddClassAttributeHandler as any).$inject = [ 'moddle' ];
