//---------------------CLASS---------------------
export default class AddClassMethodHandler {


    //---------------------CONSTRUCTOR---------------------
    constructor(private moddle) { }


    //---------------------METHODS---------------------
    execute(context) {

        const umlElement = context.element.umlElement;
        const newMethod = this.moddle.create('uml:Operation', {
            visibility: 'Public',
            returnType: 'void',
            name: 'newMethod',
            parameter: [],
            owner: umlElement
        });
        this.ensureId(newMethod);

        umlElement.get('ownedOperation').push(newMethod);

        const length = umlElement.get('ownedProperty').length + umlElement.get('ownedOperation').length;

        if (length > 4) {

            context.element.height = 100 + (length - 4) * 14;

        }

        return context.element;

    }

    private ensureId(element) {

        const prefix = (element.$type || '').replace(/^[^:]*:/g, '') + '_';

        if (!element.id) {

            element.id = this.moddle.ids.nextPrefixed(prefix, element);

        }
    }
}

(AddClassMethodHandler as any).$inject = [ 'moddle' ];
