//---------------------CLASS---------------------
export default class AddEnumLiteralHandler {


    //---------------------CONSTRUCTOR---------------------
    constructor(private moddle) { }


    //---------------------METHODS---------------------
    execute(context) {

        const umlElement = context.element.umlElement;
        const newLiteral = this.moddle.create('uml:Literal', {
            name: 'newLiteral',
            owner: umlElement
        });
        
        this.ensureId(newLiteral);

        umlElement.get('ownedLiteral').push(newLiteral);

        const length = umlElement.get('ownedLiteral').length;

        if (length > 4) {

            context.element.height = 100 + (length - 4) * 14;

        }

        return context.element;

    }

    private ensureId(element) {

        const prefix = (element.$type || '').replace(/^[^:]*:/g, '') + '_';

        if (!element.id) {

            element.id = this.moddle.ids.nextPrefixed(prefix, element);

        }
    }
}

(AddEnumLiteralHandler as any).$inject = [ 'moddle' ];
