//---------------------IMPORTS---------------------
import { assign } from 'lodash';
import { is } from '../../util/ModelUtil';
import * as LabelUtil from '../label-editing/LabelUtil';
import CommandInterceptor = require('diagram-js/lib/command/CommandInterceptor');


//---------------------CLASS---------------------
export default class UmlLabelSupport extends CommandInterceptor {


    //---------------------CONSTRUCTOR---------------------
    constructor(eventBus, modeling, umlFactory) {
        super(eventBus);

        // create external labels on shape creation
        this.on([ 'shape.create', 'connection.create' ], 'postExecute', function (e) {

            const context = e.context;
            const element = context.shape || context.connection;
            const umlElement = element.umlElement;
            let position;
            
            if(LabelUtil.hasExternalLabel(umlElement)) {

                position = LabelUtil.getAssocSourceDockTop(element.waypoints);
                modeling.createLabel(element, position, {
                    id: umlElement.id + '_sourceName_label',
                    umlElement: umlElement,
                    assocDock: 'sourceName'
                });
                
                position = LabelUtil.getAssocSourceDockBottom(element.waypoints);
                modeling.createLabel(element, position, {
                    id: umlElement.id + '_sourceCard_label',
                    umlElement: umlElement,
                    assocDock: 'sourceCard'
                });
                
                position = LabelUtil.getAssocTargetDockTop(element.waypoints);
                modeling.createLabel(element, position, {
                    id: umlElement.id + '_targetName_label',
                    umlElement: umlElement,
                    assocDock: 'targetName'
                });
                
                position = LabelUtil.getAssocTargetDockBottom(element.waypoints);
                modeling.createLabel(element, position, {
                    id: umlElement.id + '_targetCard_label',
                    umlElement: umlElement,
                    assocDock: 'targetCard'
                });
            }
        });

        // update di information on label movement and creation
        this.on([ 'label.create', 'shape.moved' ], 'executed', function (e) {

            const element = e.context.shape;
            let umlElement;
            let di;

            // we want to trigger on real labels only
            if (!element.labelTarget) {
                return;
            }

            // we want to trigger on UML elements only
            if (!is(element.labelTarget || element, 'uml:Element')) {
                return;
            }

            umlElement = element.umlElement;
            di = umlElement.di;

            if(is(element.labelTarget, 'uml:Association')) {
                
                if(!di.labels) {
                    
                    di.labels = [];
                    
                }
                
                const label = umlFactory.create('umldi:UMLLabel', {
                    bounds: umlFactory.create('dc:Bounds')
                });
                
                di.labels.push(label);

                assign(label.bounds, {
                    x: element.x,
                    y: element.y,
                    width: element.width,
                    height: element.height
                });
                
            } else {

                if (!di.label) {
                    di.label = umlFactory.create('umldi:UMLLabel', {
                        bounds: umlFactory.create('dc:Bounds')
                    });
                }

                assign(di.label.bounds, {
                    x: element.x,
                    y: element.y,
                    width: element.width,
                    height: element.height
                });
            }
        });
        
        this.on([ 'connection.layout', 'connection.reconnectEnd', 'connection.reconnectStart', 'connection.updateWaypoints' ], 'postExecute', function(e) {
            
           const labels = e.context.connection.labels;
           const connection = e.context.connection;
           let labelAdjustment;
           
           if(!labels) {
               
               return;
               
           }
           
           for(let l of labels) {
               
               if (l.assocDock === 'sourceName') {

                   const oldPosition = LabelUtil.getAssocSourceDockTop(e.context.oldWaypoints);
                   const newPosition = LabelUtil.getAssocSourceDockTop(connection.waypoints);
                   labelAdjustment = { x: newPosition.x - oldPosition.x, y: newPosition.y - oldPosition.y };

               } else if(l.assocDock === 'sourceCard') {

                   const oldPosition = LabelUtil.getAssocSourceDockBottom(e.context.oldWaypoints);
                   const newPosition = LabelUtil.getAssocSourceDockBottom(connection.waypoints);
                   labelAdjustment = { x: newPosition.x - oldPosition.x, y: newPosition.y - oldPosition.y };
                   
               } else if(l.assocDock === 'targetName') {

                   const oldPosition = LabelUtil.getAssocTargetDockTop(e.context.oldWaypoints);
                   const newPosition = LabelUtil.getAssocTargetDockTop(connection.waypoints);
                   labelAdjustment = { x: newPosition.x - oldPosition.x, y: newPosition.y - oldPosition.y };
                   
               } else if(l.assocDock === 'targetCard') {

                   const oldPosition = LabelUtil.getAssocTargetDockBottom(e.context.oldWaypoints);
                   const newPosition = LabelUtil.getAssocTargetDockBottom(connection.waypoints);
                   labelAdjustment = { x: newPosition.x - oldPosition.x, y: newPosition.y - oldPosition.y };

               }
                   
               modeling.moveShape(l, labelAdjustment);
               
           }
        });
    }
}

(UmlLabelSupport as any).$inject = [ 'eventBus', 'modeling', 'umlFactory' ];
