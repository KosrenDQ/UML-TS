//---------------------IMPORTS---------------------
import UmlSnapping from './UmlSnapping';


//---------------------EXPORTS---------------------
module.exports = {
    __init__: [ 'snapping' ],
    snapping: [ 'type', UmlSnapping ]
};
