//---------------------IMPORTS---------------------
import BaseSnapping = require('diagram-js/lib/features/snapping/Snapping');


//---------------------CLASS---------------------
export default class UmlSnapping extends BaseSnapping {


    //---------------------CONSTRUCTOR---------------------
    constructor(eventBus, canvas) {
        super(eventBus, canvas);

        eventBus.on('resize.start', 1501, function (event) {

            const context = event.context;
            context.minDimensions = {width: 80, height: 50};

        });
    }
}

(UmlSnapping as any).$inject = [ 'eventBus', 'canvas' ];
