//---------------------IMPORTS---------------------
import { assign } from 'lodash';


//---------------------CLASS---------------------
export default class PaletteProvider {


    //---------------------CONSTRUCTOR---------------------
    constructor(private palette, private create, private elementFactory, private handTool, private lassoTool, private spaceTool) {

        palette.registerProvider(this);

    }


    //---------------------METHODS---------------------
    getPaletteEntries(element) {

        const actions = {};

        assign(actions, {
            'hand-tool': {
                group: 'tools',
                className: 'bpmn-icon-hand-tool',
                title: 'Activate the hand tool',
                action: {
                    click: (event) => {
                        this.handTool.activateHand(event);
                    }
                }
            },
            'lasso-tool': {
                group: 'tools',
                className: 'bpmn-icon-lasso-tool',
                title: 'Activate the lasso tool',
                action: {
                    click: (event) => {
                        this.lassoTool.activateSelection(event);
                    }
                }
            },
            'space-tool': {
                group: 'tools',
                className: 'bpmn-icon-space-tool',
                title: 'Activate the create/remove space tool',
                action: {
                    click: (event) => {
                        this.spaceTool.activateSelection(event);
                    }
                }
            },
            'tool-separator': {
                group: 'tools',
                separator: true
            },
            'add-class': this.createAction('uml:Class', 'class', 'bpmn-icon-lane-divide-three', 'Add Class'),
            'add-enum': this.createAction('uml:Enum', 'class', 'bpmn-icon-lane-divide-two', 'Add Enumeration')

        });

        return actions;

    }

    createAction(type, group, className, title, options?) {

        const createListener = (event) => {

            const shape = this.elementFactory.createShape(assign({type: type}, options));
            this.create.start(event, shape);

        };

        const shortType = type.replace(/^uml\:/, '');

        return {
            group: group,
            className: className,
            title: title || `Create ${shortType}`,
            action: {
                dragstart: createListener,
                click: createListener
            }
        };
    }
}

(PaletteProvider as any).$inject = [ 'palette', 'create', 'elementFactory', 'handTool', 'lassoTool', 'spaceTool' ];
