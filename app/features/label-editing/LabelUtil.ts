//---------------------IMPORTS---------------------
import { assign } from 'lodash';
import { is } from '../../util/ModelUtil';


//---------------------CONSTANTS---------------------
export const DEFAULT_LABEL_SIZE = {

    width: 90,
    height: 20

};


//---------------------METHODS---------------------
export function getLabel(element, label) {

    const umlElement = element.umlElement;
    
    if(umlElement.$type === 'uml:Association' && label) {

        let text = '';
        
        if (label.assocDock === 'sourceName') {

            text = umlElement.sourceName;

        } else if(label.assocDock === 'sourceCard') {

            text = umlElement.sourceCard;

        } else if(label.assocDock === 'targetName') {

            text = umlElement.targetName;

        } else if(label.assocDock === 'targetCard') {

            text = umlElement.targetCard;

        }
        
        return text;
         
    } else {

        return umlElement.name;

    }
}

export function setLabel(element, text) {

    const umlElement = element.umlElement;
    
    if(umlElement.$type === 'uml:Class' || umlElement.$type === 'uml:Enum') {

        umlElement.name = text;

    } else if(umlElement.$type === 'uml:Association') {

        if (element.assocDock === 'sourceName') {

            umlElement.sourceName = text;

        } else if(element.assocDock === 'sourceCard') {

            umlElement.sourceCard = text;

        } else if(element.assocDock === 'targetName') {
            
            umlElement.targetName = text;
            
        } else if(element.assocDock === 'targetCard') {

            umlElement.targetCard = text;

        }
    }

    const label = element.label || element;
    label.hidden = false;

    return label;

}

export function hasExternalLabel(umlElement) {

    return is(umlElement, 'uml:Association');

}

export function getWaypointsMid(waypoints) {

    const mid = waypoints.length / 2 - 1;
    const first = waypoints[Math.floor(mid)];
    const second = waypoints[Math.ceil(mid + 0.01)];

    return {

        x: first.x + (second.x - first.x) / 2,
        y: first.y + (second.y - first.y) / 2

    };
}

export function getAssocSourceDockTop(waypoints) {
    
    const first = waypoints[0];
    
    return {

        x: first.x + 10 + DEFAULT_LABEL_SIZE.width / 2,
        y: first.y - DEFAULT_LABEL_SIZE.height / 2
        
    };
}

export function getAssocSourceDockBottom(waypoints) {

    const first = waypoints[0];

    return {

        x: first.x + 10 + DEFAULT_LABEL_SIZE.width / 2,
        y: first.y + DEFAULT_LABEL_SIZE.height / 2

    };
}

export function getAssocTargetDockTop(waypoints) {
    
    const first = waypoints[waypoints.length - 1];
    
    return {

        x: first.x - 10 - DEFAULT_LABEL_SIZE.width / 2,
        y: first.y - DEFAULT_LABEL_SIZE.height / 2
        
    };
}

export function getAssocTargetDockBottom(waypoints) {

    const first = waypoints[waypoints.length - 1];

    return {

        x: first.x - 10 - DEFAULT_LABEL_SIZE.width / 2,
        y: first.y + DEFAULT_LABEL_SIZE.height / 2

    };
}

export function getExternalLabelMid(element) {

    if (element.waypoints) {

        return getWaypointsMid(element.waypoints);

    } else {

        return {

            x: element.x + element.width / 2,
            y: element.y + element.height + DEFAULT_LABEL_SIZE.height / 2

        };
    }
}

export function getExternalLabelBounds(semantic, element) {

    let mid;
    let size;
    let bounds;
    const di = semantic.di;
    const label = di.label;

    if (label && label.bounds) {

        bounds = label.bounds;

        size = {

            width: Math.max(DEFAULT_LABEL_SIZE.width, bounds.width),
            height: bounds.height

        };

        mid = {

            x: bounds.x + bounds.width / 2,
            y: bounds.y + bounds.height / 2

        };

    } else {

        mid = getExternalLabelMid(element);
        size = DEFAULT_LABEL_SIZE;

    }

    return assign({

        x: mid.x - size.width / 2,
        y: mid.y - size.height / 2

    }, size);
}
