//---------------------IMPORTS---------------------
import * as LabelUtil from './LabelUtil';


//---------------------CLASS---------------------
export default class UpdateLabelHandler {


    //---------------------METHODS---------------------
    execute(ctx) {
        
        const context = ctx.element;
        
        if(!context.label) {

            ctx.oldLabel = LabelUtil.getLabel(ctx.element, null);
            

        } else {
            
            ctx.oldLabel = LabelUtil.getLabel(context.element, context.label);
            
        }

        return this.setText(ctx.element, ctx.newLabel);

    }

    revert(ctx) {

        return this.setText(ctx.element, ctx.oldLabel);

    }

    setText(element, text) {

        // external label if present
        const label = element.label || element;
        const labelTarget = element.labelTarget || element;

        LabelUtil.setLabel(label, text);

        return [ label, labelTarget ];

    }
}
