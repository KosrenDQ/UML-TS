//---------------------IMPORTS---------------------
import UpdateLabelHandler from './UpdateLabelHandler';
import * as LabelUtil from './LabelUtil';


//---------------------CONSTANTS---------------------
const MIN_BOUNDS = {
    width: 150,
    height: 50
};


//---------------------CLASS---------------------
export default class LabelEditingProvider {


    //---------------------CONSTRUCTOR---------------------
    constructor(eventBus, private canvas, directEditing, private commandStack) {

        directEditing.registerProvider(this);
        this.commandStack.registerHandler('element.updateLabel', UpdateLabelHandler);

        eventBus.on('element.dblclick', function(event) {

            directEditing.activate(event.element);

        });

        eventBus.on([ 'element.mousedown', 'drag.init', 'canvas.viewbox.changed' ], function(event) {

            directEditing.complete();

        });

        eventBus.on([ 'commandStack.changed' ], function() {

            directEditing.cancel();

        });

        eventBus.on('create.end', 500, function(e) {

            const element = e.shape;
            const canExecute = e.context.canExecute;

            if (!canExecute) {
                return;
            }

            directEditing.activate(element);

        });
    }


    //---------------------METHODS---------------------
    activate(element) {

        const text = LabelUtil.getLabel(element, null);

        if (text === undefined) {

            return;

        }

        const properties = this.getEditingBBox(element);
        properties.text = text;
        return properties;

    }

    getEditingBBox(element) {

        const target = element.label || element;
        const bbox = this.canvas.getAbsoluteBBox(target);
        const mid = {
            x: bbox.x + bbox.width / 2,
            y: bbox.y + bbox.height / 2
        };
        
        const bounds = { x: bbox.x, y: bbox.y, width: bbox.width, height: bbox.height, text: '' };
        
        // external label
        if(target.labelTarget) {

            bounds.width = Math.max(bbox.width, MIN_BOUNDS.width);
            bounds.height = Math.max(bbox.height, MIN_BOUNDS.height);
            bounds.x = mid.x - bounds.width / 2;

        }

        return { bounds: bounds, style: '', text: '' };

    }

    update(element, newLabel) {

        this.commandStack.execute('element.updateLabel', {

            element: element,
            newLabel: newLabel

        });
    }
}

(LabelEditingProvider as any).$inject = [ 'eventBus', 'canvas', 'directEditing', 'commandStack' ];
