//---------------------IMPORTS---------------------
import RuleProvider = require('diagram-js/lib/features/rules/RuleProvider');
import { Root } from 'diagram-js/lib/model/index';
import { is } from '../../util/ModelUtil';


//---------------------GLOBAL_METHODS---------------------
// from https://github.com/bpmn-io/diagram-js/blob/master/test/spec/features/move/rules/MoveRules.js
function notMoveOnItself(shapes, target): boolean {

    while(target) {

        if(shapes.indexOf(target) !== -1) {

            return false;

        }

        target = target.parent;

    }

    return true;

}

function notMoveLabel(shapes, target): boolean {

    return target.type !== 'label';

}

function canResize(shape, newBounds) {

    if (is(shape, 'uml:Association') || is(shape, 'uml:SuperClass') || is(shape, 'uml:Relation')) {

        return false;

    }

    return !newBounds || (newBounds.width >= 50 && newBounds.height >= 50);

}


//---------------------CLASS---------------------
export default class UmlRules extends RuleProvider {


    //---------------------CONSTRUCTOR---------------------
    constructor(eventBus) {
        super(eventBus);
    }


    //---------------------METHOD---------------------
    init() {

        this.addRule('elements.move', function (context) {

            const target = context.target;
            const shapes = context.shapes;

            return !target || (notMoveOnItself(shapes, target) && notMoveLabel(shapes, target) && target instanceof Root);

        });

        this.addRule('shape.resize', function(context) {

            const shape = context.shape;
            const newBounds = context.newBounds;

            return canResize(shape, newBounds);

        });
    }

    canConnect(source, target): any {
        
        if(source.superClass) {
            
            if(source !== target && is(target, 'uml:Class')) {
                
                return { type: 'uml:SuperClass' };
                
            }
            
        } else {
            
            if(source !== target && is(target, 'uml:Enum') && source.enumLink) {

                return { type: 'uml:Relation' };
                
            } else if(is(target, 'uml:Class')) {

                return { type: 'uml:Association' };
                
            }
        }
        
        return false;

    }
}

(UmlRules as any).$inject = [ 'eventBus' ];
