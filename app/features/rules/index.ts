//---------------------IMPORTS---------------------
import UmlRules from './UmlRules';


//---------------------EXPORTS---------------------
module.exports = {

    __depends__: [
        require('diagram-js/lib/features/rules')
    ],
    __init__: [ 'umlRules' ],
    umlRules: [ 'type', UmlRules ]

};
