//---------------------API---------------------
export function is(element, type) {

    const ue = getUmlElement(element);
    return ue && (typeof ue.$instanceOf === 'function') && ue.$instanceOf(type);

}

export function getUmlElement(element) {

    return (element && element.umlElement) || element;

}
