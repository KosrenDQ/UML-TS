//---------------------IMPORTS---------------------
import { assign } from 'lodash';
import { Reader as XMLReader, Writer as XMLWriter } from 'moddle-xml';
import * as Moddle from 'moddle';
import Ids = require('ids');
import SimpleLayouter from './layout/SimpleLayouter';


//---------------------CONSTANTS---------------------
const DEFAULT_PACKAGES: Moddle.Package[] = [

    require('../resources/class.json'),
    require('../resources/classdi.json'),
    require('../resources/dc.json'),
    require('../resources/di.json')

];


//---------------------CLASS--------------------
export default class UmlModdle extends Moddle {


    //---------------------ATTRIBUTES---------------------
    public ids: Ids;
    private layouter: SimpleLayouter;
    

    //---------------------CONSTRUCTOR---------------------
    constructor(packages: Moddle.Package[] = DEFAULT_PACKAGES) {

        super(packages);
        
        this.layouter = new SimpleLayouter();

    }


    //---------------------METHODS---------------------
    public fromXML(xmlStr: string, typeName: string = 'uml:ClassGraph', options = {}, done: () => void = () => {}): void {

        const reader = new XMLReader(assign({model: this, lax: true}, options));
        const rootHandler = reader.handler(typeName);
        reader.fromXML(xmlStr, rootHandler, done);

    }

    public fromJSONToXML(json: string, done: (event: any, result?: any) => void = () => {}): void {

        try {

            let xml = '';
            let xmlDi = '';
            let graph = JSON.parse(json);
            this.layouter.clearLayouter();
            
            if(graph.type === 'class') {
                
                xml += `<uml:ClassGraph 
                            xmlns:uml="http://seblog.cs.uni-kassel.de/uml" 
                            xmlns:umldi="http://seblog.cs.uni-kassel.de/umldi" 
                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                            xmlns:di="http://www.omg.org/spec/DD/20100524/DI" 
                            xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" 
                            id="ClassGraph_imported">`;
                
                xmlDi = `<umldi:UMLDiagram id="UMLDiagram_1">`;
                
                for(let node of graph.nodes) {
                    
                    if(node.type === 'node') {

                        xml += `<uml:Class id="Class_${node.id}" name="${node.id}" isAbstract="false" isInterface="false">`;
                        xmlDi += `<umldi:UMLShape id="Class_${node.id}_di" umlElement="Class_${node.id}">
                                    ${this.layouter.layoutNodeSideBySide(node)}
                                  </umldi:UMLShape>`;
                        
                        for(let attribute of node.attributes) {

                            const attrName = attribute.split(':')[0].trim();
                            const attrType = attribute.split(':')[1].trim();
                            
                            xml += `<uml:Property id="Property_${graph.nodes.indexOf(node)}_${node.attributes.indexOf(attribute)}" owner="Class_${node.id}" visibility="Private" name="${attrName}" type="${attrType}"/>`;
                            
                        } 
                        
                        for(let method of node.methods) {
                            
                            const name = method.substring(0, method.indexOf('('));
                            const returnType = method.split(') ')[1].trim();
                            
                            xml += `<uml:Operation id="Operation_${graph.nodes.indexOf(node)}_${node.methods.indexOf(method)}" owner="Class_${node.id}" visibility="Public" returnType="${returnType}" name="${name}">`;
                            
                            const parameterString = method.substring(method.indexOf('(') + 1, method.indexOf(')'));
                            
                            if(parameterString && parameterString.length > 0) {
                                
                                const parameters = parameterString.split(',');
                                
                                for(let param of parameters) {
                             
                                    const paramType = param.split(' ')[0];
                                    const paramName = param.split(' ')[1];
                                    
                                    xml += `<uml:Property id="Property_${graph.nodes.indexOf(node)}_${node.methods.indexOf(method)}_${parameters.indexOf(param)}" name="${paramName}" type="${paramType}"/>`;
                                    
                                } 
                            }
                            
                            xml += `</uml:Operation>`;
                            
                        }
                        
                        xml += `</uml:Class>`;       
                        
                    }
                }
                
                for(let edge of graph.edges) {
                    
                    if(edge.type === 'ASSOCIATION') {
                        
                        xml += `<uml:Association id="Association_${graph.edges.indexOf(edge)}" sourceClass="Class_${edge.source.id}" sourceName="${edge.source.property}" sourceCard="${edge.source.cardinality}" targetClass="Class_${edge.target.id}" targetName="${edge.target.property}" targetCard="${edge.target.cardinality}"/>`;
                        xmlDi += `<umldi:UMLEdge id="Association_${graph.edges.indexOf(edge)}_di" umlElement="Association_${graph.edges.indexOf(edge)}">
                                    ${this.layouter.layoutEdgeSideBySide(edge)}
                                    ${this.layouter.layoutLabelSideBySide(edge, graph.edges.indexOf(edge))}
                                  </umldi:UMLEdge>`;
                        
                    } else if(edge.type === 'GENERALISATION') {
                        
                        xml += `<uml:SuperClass id="SuperClass_${graph.edges.indexOf(edge)}" childClass="Class_${edge.source.id}" fatherClass="Class_${edge.target.id}"/>`;
                        xmlDi += `<umldi:UMLEdge id="SuperClass_${graph.edges.indexOf(edge)}_di" umlElement="SuperClass_${graph.edges.indexOf(edge)}">
                                    ${this.layouter.layoutEdgeSideBySide(edge)}
                                  </umldi:UMLEdge>`;
                        
                    }
                }

                xmlDi += `</umldi:UMLDiagram>`;
                
                xml += `${xmlDi}
                        </uml:ClassGraph>`;
                
            }
            
            done(null, xml);

        } catch(e) {

            done(e);

        }
    }
    public toXML(element, options = {}, done: (event: any, result?: any) => void = () => {}): void {

        const writer = new XMLWriter(options);

        try {

            const result = writer.toXML(element);
            done(null, result);

        } catch(e) {

            done(e);

        }
    }

    public toJSON(element, done: (event: any, result?: any) => void = () => {}): void {

        try {

            let result = '';

            if(element.$instanceOf('uml:ClassGraph')) {

                result += `{ "type": "class", "nodes": [`;
                
                for(let umlClass of element.get('ownedClass')) {

                    result += `{ "type": "node", "id": "${umlClass.name}", "attributes": [`;

                    for(let property of umlClass.get('ownedProperty')) {

                        result += `"${property.name}: ${property.type}"`;

                        if(umlClass.get('ownedProperty').indexOf(property) < umlClass.get('ownedProperty').length - 1) {

                            result += `, `;

                        }
                    }
                    
                    result += `], "methods": [`;
                    
                    for(let operation of umlClass.get('ownedOperation')) {

                        result += `"${operation.name}(`;

                        for(let parameter of operation.get('parameter')) {

                            result += `${parameter.type} ${parameter.name}`;
                            
                            if(operation.get('parameter').indexOf(parameter) < operation.get('parameter').length - 1) {

                                result += `, `;

                            }
                        }

                        result += `)`;

                        if(operation.returnType !== 'void') {

                            result += ` ${operation.returnType}`;

                        }
                        
                        result += `"`;
                        
                        if(umlClass.get('ownedOperation').indexOf(operation) < umlClass.get('ownedOperation').length - 1) {

                            result += `, `;

                        }
                    }

                    result += `]}`;

                    if(element.get('ownedClass').indexOf(umlClass) < element.get('ownedClass').length - 1) {

                        result += `, `;

                    }
                }

                result += `], "edges": [`;

                for(let association of element.get('ownedAssociation')) {

                    result += `{ "type": "ASSOCIATION", 
                                "source": { "id": "${association.sourceClass.name}", "cardinality": "${association.sourceCard}", "property": "${association.sourceName}" }, 
                                "target": { "id": "${association.targetClass.name}", "cardinality": "${association.targetCard}", "property": "${association.targetName}" } }`;
                    
                    if(element.get('ownedAssociation').indexOf(association) < element.get('ownedAssociation').length - 1) {

                        result += `, `;

                    }
                }
                
                if(element.get('ownedSuperClass').length > 1) {
                    
                    result += `, `;
                    
                }

                for(let superClass of element.get('ownedSuperClass')) {

                    result += `{ "type": `;
                    
                    if(superClass.fatherClass.isInterface) {
                        
                        result += `"IMPLEMENTS"`;

                    } else {

                        result += `"GENERALISATION"`;
                        
                    }
                    
                    result += `, "source": { "id": "${superClass.childClass.name}", "cardinality": "one", "property": "${superClass.childClass.name}" }, 
                                "target": { "id": "${superClass.fatherClass.name}", "cardinality": "one", "property": "${superClass.fatherClass.name}" } }`;

                    
                    if(element.get('ownedSuperClass').indexOf(superClass) < element.get('ownedSuperClass').length - 1) {

                        result += `, `;

                    }
                }
                
                result += `]}`;

            }

            done(null, result);

        } catch(e) {

            done(e);

        }
    }
}
